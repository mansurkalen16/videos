@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Добавление видео к {{$course->name}}</div>

                    <div class="card-body">
                        <form id="createForm"
                              action="{{ action('VideoController@store') }}"
                              enctype="multipart/form-data" method="post">
                            {{ csrf_field() }}
                            <input   type="hidden" name="course_id" value="{{$course->id}}" required />
                            <label for="name">Название видео</label>
                            <input class="form-control" id="name" type="text" name="name" required />
                            <br>
                            <label for="link">Ссылка с youtube</label>
                            <input class="form-control" id="link" type="text" name="link" required />
                            <br>
                            <button class="btn btn-success">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
