@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Курсы
                        @if(auth()->user()->role_id == 1)
                            <a href="{{ action('CourseController@create')}}" class="btn btn-primary float-right" >Добавить курс</a>
                        @endif
                    </div>

                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>Количество видео</th>

                                @if(auth()->user()->role_id == 1)
                                    <th>Действия</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($courses))
                                @foreach($courses as $course)
                                    <tr>
                                        <td>
                                            <a href="/courses/{{$course->id}}"   >{{$course->name}}</a>
                                        </td>
                                        <td>{{count($course->videos)}}</td>

                                        @if(auth()->user()->role_id == 1)
                                            <td><a class="btn btn-primary" href="{{ action('VideoController@create', ['id' => $course->id])}}" >Добавить видео</a></td>
                                        @endif
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3" class="text-center">Нет курсов!</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
