@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Добавление курса</div>

                <div class="card-body">
                    <form id="createForm"
                          action="{{ action('CourseController@store') }}"
                          enctype="multipart/form-data" method="post">
                    {{ csrf_field() }}
                        <label for="name">Название курса</label>
                        <input class="form-control" id="name" type="text" name="name" required />
                        <br>
                        <button class="btn btn-success">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
