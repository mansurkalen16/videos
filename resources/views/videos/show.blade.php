@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Курс {{$course->name}}

                        @if(auth()->user()->role_id == 1)
                            <a class="btn btn-primary float-right" href="{{ action('VideoController@create', ['id' => $course->id])}}" >Добавить видео</a>
                        @endif
                    </div>

                    <div class="card-body">
                        <div class="col-md-12">
                            @if(count($videos))
                                @foreach($videos as $video)
                                    {{$video->name}}
                                    <iframe width="100%" height="400"
                                            src="{{$video->link}}">
                                    </iframe>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3" class="text-center">Нет видео!</td>
                                </tr>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
