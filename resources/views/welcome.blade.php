
<!DOCTYPE html>
<html>
  <head>
	<meta charset="utf-8" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />    	
	<!--metatextblock-->
	<title>Лекториум</title>
	<meta name="description" content="Онлайн-курсы и медиатека видеолекций на русском языке." />	
    <link rel="canonical" href="http://lektorium.tv/">

	<meta property="og:url" content="http://lektorium.tv" />
	<meta property="og:title" content="Лекториум" />
	<meta property="og:description" content="Онлайн-курсы и медиатека видеолекций на русском языке." />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="https://static.tildacdn.com/tild6532-3034-4037-b861-366530623838/-/resize/504x/_intro_110px.png" />

		<!--/metatextblock-->

    <meta property="fb:app_id" content="257953674358265" />

	<meta name="format-detection" content="telephone=no" />
	<meta http-equiv="x-dns-prefetch-control" content="on">
	<link rel="dns-prefetch" href="https://tilda.ws">
	<link rel="dns-prefetch" href="https://static.tildacdn.com">


		
	<link rel="shortcut icon" href="https://static.tildacdn.com/tild3665-6166-4363-b265-353436376161/favicon.ico" type="image/x-icon" />

	
			<link rel="stylesheet" href="https://static.tildacdn.com/css/tilda-grid-3.0.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="https://tilda.ws/project443196/tilda-blocks-2.12.css?t=1560773730" type="text/css" media="all" />

	<link rel="stylesheet" href="https://static.tildacdn.com/css/tilda-animation-1.0.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="https://static.tildacdn.com/css/tilda-slds-1.4.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="https://static.tildacdn.com/css/tilda-zoom-2.0.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="https://static.tildacdn.com/css/tooltipster.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="https://static.tildacdn.com/css/tilda-popup-1.1.min.css" type="text/css" media="all" />


	<script src="https://static.tildacdn.com/js/jquery-1.10.2.min.js"></script>
	<script src="https://static.tildacdn.com/js/tilda-scripts-2.8.min.js"></script>
	<script src="https://tilda.ws/project443196/tilda-blocks-2.7.js?t=1560773730"></script>

	<script src="https://static.tildacdn.com/js/tilda-animation-1.0.min.js" charset="utf-8"></script>
	<script src="https://static.tildacdn.com/js/tilda-slds-1.4.min.js" charset="utf-8"></script>
	<script src="https://static.tildacdn.com/js/hammer.min.js" charset="utf-8"></script>
	<script src="https://static.tildacdn.com/js/tilda-zoom-2.0.min.js" charset="utf-8"></script>
	<script src="https://static.tildacdn.com/js/jquery.tooltipster.min.js" charset="utf-8"></script>
	<script src="https://static.tildacdn.com/js/tilda-forms-1.0.min.js" charset="utf-8"></script>

	
		
		

<meta name="yandex-verification" content="861ab4d00f51d1d5" /><script type="text/javascript">window.dataLayer = window.dataLayer || [];</script>


</head>

<body class="t-body" style="margin:0;">

	<!--allrecords-->
<div id="allrecords" class="t-records" data-hook="blocks-collection-content-node" data-tilda-project-id="443196" data-tilda-page-id="1732316"  data-tilda-formskey="837688c9ca48e8cc3a88abe8957de2ba"   >

<div id="rec34747130" class="r t-rec" style="background-color:#24262f; " data-animationappear="off" data-record-type="257"   data-bg-color="#24262f">








<!-- T228 -->
<div id="nav34747130marker"></div>
  <div class="t228__mobile">
    <div class="t228__mobile_container">
      <div class="t228__mobile_text t-name t-name_md" field="text">&nbsp;</div> 
      <div class="t228__burger">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>  
    </div>
  </div>
<div id="nav34747130" class="t228 t228__hidden t228__positionstatic " style="background-color: rgba(0,0,0,0.50); height:50px; " data-bgcolor-hex="#000000" data-bgcolor-rgba="rgba(0,0,0,0.50)" data-navmarker="nav34747130marker" data-appearoffset="" data-bgopacity-two="50" data-menushadow="" data-bgopacity="0.50"  data-bgcolor-rgba-afterscroll="rgba(0,0,0,0.50)" data-menu-items-align="center" data-menu="yes">
    <div class="t228__maincontainer " style="height:50px;">
          <div class="t228__padding40px"></div>
          
          <div class="t228__leftside">
                        <div class="t228__leftcontainer">    
                <a href="#"  style="color:#ffffff;"><img src="https://static.tildacdn.com/tild6532-3034-4037-b861-366530623838/_intro_110px.png" class="t228__imglogo t228__imglogomobile" imgfield="img" style="max-width: 110px;width: 110px; height: auto; display: block;" alt=""></a>            </div>
                      </div>
        
          <div class="t228__centerside ">
                        <div class="t228__centercontainer">
            <ul class="t228__list t228__list_hidden">
                    
                        </ul>
            </div>
                      </div>
          
          <div class="t228__rightside">
                          <div class="t228__rightcontainer">
                                                            
                                    <div class="t228__right_buttons">
                        <div class="t228__right_buttons_wrap">
                          <div class="t228__right_buttons_but"><a href="{{ route('login') }}" target="" class="t-btn "  style="color:#ffffff;border:1px solid #ffffff;background-color:#000000;border-radius:0px; -moz-border-radius:0px; -webkit-border-radius:0px;"><table style="width:100%; height:100%;"><tr><td>Личный кабинет</td></tr></table></a></div>
                        </div>
                    </div>
                                        
             </div>
                       </div>
          
          <div class="t228__padding40px"></div>    
  </div>
</div>



<style>
@media screen and (max-width: 980px) {
  #rec34747130 .t228__leftcontainer{
    padding: 20px;
  }
}
@media screen and (max-width: 980px) {
  #rec34747130 .t228__imglogo{
    padding: 20px 0;
  }
}
</style>


<script type="text/javascript">
    
      $(document).ready(function() {
        t228_highlight();
        
                        
      }); 
    

    
    $(window).resize(function() {
      t228_setWidth('34747130');
    });
    $(window).load(function() {
      t228_setWidth('34747130');
    });
    $(document).ready(function() {
      t228_setWidth('34747130');
    });        
    
 


$(window).resize(function() {
    t228_setBg('34747130');
});
$(document).ready(function() {
    t228_setBg('34747130');
});     




</script>

<script type="text/javascript">
      
        $(document).ready(function() {
            t228_createMobileMenu('34747130');
        }); 
    
</script>

      
  

<!--[if IE 8]>
<style>
#rec34747130 .t228 {
  filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#D9000000', endColorstr='#D9000000');
}
</style>
<![endif]-->

</div>


<div id="rec39222441" class="r t-rec" style=" " data-animationappear="off" data-record-type="131"   >
<!-- T123 -->
<div class="t123" style="position: absolute; width: 1px; height: 1px; opacity:0;">
<div class="t-container_100 ">
<div class="t-width t-width_100 ">
<!-- nominify begin -->
<script>
  window.intercomSettings = {
    app_id: "zi3y51lh"
  };
</script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/zi3y51lh';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
 
<!-- nominify end -->
</div>  
</div>
</div>



</div>


<div id="rec93248251" class="r t-rec" style=" " data-animationappear="off" data-record-type="131"   >
<!-- T123 -->
<div class="t123" style="position: absolute; width: 1px; height: 1px; opacity:0;">
<div class="t-container_100 ">
<div class="t-width t-width_100 ">
<!-- nominify begin -->
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter24232456 = new Ya.Metrika({
                    id:24232456,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/24232456" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
 
<!-- nominify end -->
</div>  
</div>
</div>



</div>


<div id="rec34711104" class="r t-rec t-rec_pt_0 t-rec_pb_0 t-screenmax-640px" style="padding-top:0px;padding-bottom:0px; "  data-record-type="388"  data-screen-max="640px" >



<!-- t388 -->
<!-- cover -->
	




<div class="t-cover" id="recorddiv34711104" bgimgfield="img" style="height:600px; background-image:-webkit-linear-gradient(top, #ccc, #777); background-image:-moz-linear-gradient(top, #ccc, #777); background-image:-o-linear-gradient(top, #ccc, #777); background-image:-ms-linear-gradient(top, #ccc, #777); background-image:linear-gradient(top, #ccc, #777); " >

	<div class="t-cover__carrier" id="coverCarry34711104" data-content-cover-id="34711104"  data-content-cover-bg="https://static.tildacdn.com/tild6432-6361-4433-a233-366431333938/top622259.jpg" data-content-cover-height="600px" data-content-cover-parallax="fixed"        style="background-image:url('https://static.tildacdn.com/tild6432-6361-4433-a233-366431333938/top622259.jpg');height:600px; "></div>

    <div class="t-cover__filter" style="height:600px;background-image: -moz-linear-gradient(top, rgba(0,0,0,0.20), rgba(0,0,0,0.90));background-image: -webkit-linear-gradient(top, rgba(0,0,0,0.20), rgba(0,0,0,0.90));background-image: -o-linear-gradient(top, rgba(0,0,0,0.20), rgba(0,0,0,0.90));background-image: -ms-linear-gradient(top, rgba(0,0,0,0.20), rgba(0,0,0,0.90));background-image: linear-gradient(top, rgba(0,0,0,0.20), rgba(0,0,0,0.90));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#cc000000', endColorstr='#19000000');"></div>
<div class="t388">
  <div class="t-container">
    <div class="">
      <div class="t-cover__wrapper t-valign_bottom" style="height:600px;"> 
        <div class="t388__wrapper" data-hook-content="covercontent">
           <div class="t388__textwrapper t-width t-width_6">
           <div class="t388__uptitle t-uptitle t-uptitle_md" style="" field="subtitle"><div style="font-family:'Open Sans';" data-customstyle="yes">Просветительский проект</div></div>           <div class="t388__title t-section__title t-title t-title_md" style="" field="title"><div style="font-size:72px;font-family:'Intro';" data-customstyle="yes"><span style="font-weight: 700;">ЛЕКТОРИУМ</span></div></div>                      </div>
                       <div class="t-col t388__col_3 t-col_3 ">
                <a href="#"  >                <img class="t388__img t-img" src="https://static.tildacdn.com/tild3534-3336-4139-a636-623864306662/noroot.svg" imgfield="img1"  data-hook-clogo="coverlogo"/>
                </a>            </div>
                                    <div class="t-col t388__col_3 t-col_3 ">
                <a href="#"  >                <img class="t388__img t-img" src="https://static.tildacdn.com/tild6636-6464-4033-a530-346262346435/noroot.svg" imgfield="img2"  data-hook-clogo="coverlogo"/>
                </a>            </div>
                                    <div class="t-col t388__col_3 t-col_3 ">
                <a href="#"  >                <img class="t388__img t-img" src="https://static.tildacdn.com/tild3638-3662-4239-b930-646431333530/noroot.svg" imgfield="img3"  data-hook-clogo="coverlogo"/>
                </a>            </div>
                                    <div class="t-col t388__col_3 t-col_3 ">
                <a href="#"  >                <img class="t388__img t-img" src="https://static.tildacdn.com/tild3738-6138-4330-a564-353839333232/noroot.svg" imgfield="img4"  data-hook-clogo="coverlogo"/>
                </a>            </div>
                                </div>
      </div>
    </div>
  </div>
</div>
  

</div>
    
</div>


<div id="rec37330387" class="r t-rec t-rec_pt_0 t-rec_pb_0 t-screenmin-1200px" style="padding-top:0px;padding-bottom:0px; "  data-record-type="388" data-screen-min="1200px"  >



<!-- t388 -->
<!-- cover -->
	




<div class="t-cover" id="recorddiv37330387" bgimgfield="img" style="height:600px; background-image:-webkit-linear-gradient(top, #ccc, #777); background-image:-moz-linear-gradient(top, #ccc, #777); background-image:-o-linear-gradient(top, #ccc, #777); background-image:-ms-linear-gradient(top, #ccc, #777); background-image:linear-gradient(top, #ccc, #777); " >

	<div class="t-cover__carrier" id="coverCarry37330387" data-content-cover-id="37330387"  data-content-cover-bg="https://static.tildacdn.com/tild6432-6361-4433-a233-366431333938/top622259.jpg" data-content-cover-height="600px" data-content-cover-parallax="fixed"        style="background-image:url('https://static.tildacdn.com/tild6432-6361-4433-a233-366431333938/top622259.jpg');height:600px; "></div>

    <div class="t-cover__filter" style="height:600px;background-image: -moz-linear-gradient(top, rgba(0,0,0,0.20), rgba(0,0,0,0.90));background-image: -webkit-linear-gradient(top, rgba(0,0,0,0.20), rgba(0,0,0,0.90));background-image: -o-linear-gradient(top, rgba(0,0,0,0.20), rgba(0,0,0,0.90));background-image: -ms-linear-gradient(top, rgba(0,0,0,0.20), rgba(0,0,0,0.90));background-image: linear-gradient(top, rgba(0,0,0,0.20), rgba(0,0,0,0.90));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#cc000000', endColorstr='#19000000');"></div>
<div class="t388">
  <div class="t-container">
    <div class="">
      <div class="t-cover__wrapper t-valign_bottom" style="height:600px;"> 
        <div class="t388__wrapper" data-hook-content="covercontent">
           <div class="t388__textwrapper t-width t-width_12">
           <div class="t388__uptitle t-uptitle t-uptitle_md" style="" field="subtitle"><div style="font-family:'Open Sans';" data-customstyle="yes">Просветительский проект</div></div>           <div class="t388__title t-section__title t-title t-title_md" style="" field="title"><div style="font-size:72px;font-family:'Intro';" data-customstyle="yes"><span style="font-weight: 700;">ЛЕКТОРИУМ</span></div></div>                      </div>
                       <div class="t-col t388__col_3 t-col_3 ">
                <a href="#"  >                <img class="t388__img t-img" src="https://static.tildacdn.com/tild3534-3336-4139-a636-623864306662/noroot.svg" imgfield="img1"  data-hook-clogo="coverlogo"/>
                </a>            </div>
                                    <div class="t-col t388__col_3 t-col_3 ">
                <a href="#medialibrary"  >                <img class="t388__img t-img" src="https://static.tildacdn.com/tild6636-6464-4033-a530-346262346435/noroot.svg" imgfield="img2"  data-hook-clogo="coverlogo"/>
                </a>            </div>
                                    <div class="t-col t388__col_3 t-col_3 ">
                <a href="#"  >                <img class="t388__img t-img" src="https://static.tildacdn.com/tild3638-3662-4239-b930-646431333530/noroot.svg" imgfield="img3"  data-hook-clogo="coverlogo"/>
                </a>            </div>
                                    <div class="t-col t388__col_3 t-col_3 ">
                <a href="#"  >                <img class="t388__img t-img" src="https://static.tildacdn.com/tild3738-6138-4330-a564-353839333232/noroot.svg" imgfield="img4"  data-hook-clogo="coverlogo"/>
                </a>            </div>
                                </div>
      </div>
    </div>
  </div>
</div>
  

</div>
    
</div>


<div id="rec36940315" class="r t-rec t-screenmin-640px t-screenmax-1200px" style=" "  data-record-type="396" data-screen-min="640px" data-screen-max="1200px" >
<!-- T396 -->
<style>#rec36940315 .t396__artboard{height: 700px;background-color: #ffffff;}#rec36940315 .t396__filter{height: 700px;background-image: -webkit-gradient( linear, left top, left bottom, from(rgba(0,0,0,0.6)), to(rgba(0,0,0,0.6)) );background-image: -webkit-linear-gradient(top, rgba(0,0,0,0.6), rgba(0,0,0,0.6));background-image: linear-gradient(to bottom, rgba(0,0,0,0.6), rgba(0,0,0,0.6));}#rec36940315 .t396__carrier{height: 700px;background-position: center center;background-attachment: scroll;background-image: url('https://static.tildacdn.com/tild3832-3061-4166-a637-343535626437/top622259.jpg');background-size:cover;background-repeat:no-repeat;}@media screen and (max-width: 1199px){#rec36940315 .t396__artboard{height: 680px;background-color: #ffffff;}#rec36940315 .t396__filter{height: 680px;background-image: -webkit-gradient( linear, left top, left bottom, from(rgba(0,0,0,0.6)), to(rgba(0,0,0,0.6)) );background-image: -webkit-linear-gradient(top, rgba(0,0,0,0.6), rgba(0,0,0,0.6));background-image: linear-gradient(to bottom, rgba(0,0,0,0.6), rgba(0,0,0,0.6));}#rec36940315 .t396__carrier{height: 680px;background-position: center center;background-image: url('https://static.tildacdn.com/tild6662-3234-4239-b330-376333343666/top622259_1.jpg');background-attachment:scroll;}}@media screen and (max-width: 959px){#rec36940315 .t396__artboard{height: 680px;}#rec36940315 .t396__filter{height: 680px;background-image: -webkit-gradient( linear, left top, left bottom, from(rgba(0,0,0,0.6)), to(rgba(0,0,0,0.6)) );background-image: -webkit-linear-gradient(top, rgba(0,0,0,0.6), rgba(0,0,0,0.6));background-image: linear-gradient(to bottom, rgba(0,0,0,0.6), rgba(0,0,0,0.6));}#rec36940315 .t396__carrier{height: 680px;}}@media screen and (max-width: 639px){#rec36940315 .t396__artboard{height: 570px;}#rec36940315 .t396__filter{height: 570px;}#rec36940315 .t396__carrier{height: 570px;}}@media screen and (max-width: 479px){#rec36940315 .t396__artboard{height: 900px;}#rec36940315 .t396__filter{height: 900px;}#rec36940315 .t396__carrier{height: 900px;}}#rec36940315 .tn-elem[data-elem-id="1509617409307"]{z-index:1;top: 451px;left: calc(50% - 600px + 150px);width:155px;}#rec36940315 .tn-elem[data-elem-id="1509617409307"] .tn-atom{background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec36940315 .tn-elem[data-elem-id="1509617409307"]{top: 251px;left: calc(50% - 480px + 270px);width:155px;}}@media screen and (max-width: 959px){#rec36940315 .tn-elem[data-elem-id="1509617409307"]{top: 250px;left: calc(50% - 320px + 140px);width:155px;}}@media screen and (max-width: 639px){#rec36940315 .tn-elem[data-elem-id="1509617409307"]{top: 210px;left: calc(50% - 240px + 50px);}}@media screen and (max-width: 479px){#rec36940315 .tn-elem[data-elem-id="1509617409307"]{top: 160px;left: calc(50% - 160px + 80px);}}#rec36940315 .tn-elem[data-elem-id="1509617420525"]{z-index:2;top: 416px;left: calc(50% - 600px + 390px);width:145px;}#rec36940315 .tn-elem[data-elem-id="1509617420525"] .tn-atom{background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec36940315 .tn-elem[data-elem-id="1509617420525"]{top: 410px;left: calc(50% - 480px + 270px);}}@media screen and (max-width: 959px){#rec36940315 .tn-elem[data-elem-id="1509617420525"]{top: 410px;left: calc(50% - 320px + 140px);width:145px;}}@media screen and (max-width: 639px){#rec36940315 .tn-elem[data-elem-id="1509617420525"]{top: 370px;left: calc(50% - 240px + 50px);}}@media screen and (max-width: 479px){#rec36940315 .tn-elem[data-elem-id="1509617420525"]{top: 310px;left: calc(50% - 160px + 80px);}}#rec36940315 .tn-elem[data-elem-id="1509617431460"]{z-index:4;top: 416px;left: calc(50% - 600px + 630px);width:155px;}#rec36940315 .tn-elem[data-elem-id="1509617431460"] .tn-atom{background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec36940315 .tn-elem[data-elem-id="1509617431460"]{top: 418px;left: calc(50% - 480px + 530px);width:150px;}#rec36940315 .tn-elem[data-elem-id="1509617431460"] .tn-atom{opacity:1;border-color:#ffffff;}}@media screen and (max-width: 959px){#rec36940315 .tn-elem[data-elem-id="1509617431460"]{top: 418px;left: calc(50% - 320px + 339px);}}@media screen and (max-width: 639px){#rec36940315 .tn-elem[data-elem-id="1509617431460"]{top: 378px;left: calc(50% - 240px + 279px);}}@media screen and (max-width: 479px){#rec36940315 .tn-elem[data-elem-id="1509617431460"]{top: 678px;left: calc(50% - 160px + 85px);}}#rec36940315 .tn-elem[data-elem-id="1509617440890"]{z-index:3;top: 448px;left: calc(50% - 600px + 878px);width:153px;}#rec36940315 .tn-elem[data-elem-id="1509617440890"] .tn-atom{background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec36940315 .tn-elem[data-elem-id="1509617440890"]{top: 248px;left: calc(50% - 480px + 518px);}}@media screen and (max-width: 959px){#rec36940315 .tn-elem[data-elem-id="1509617440890"]{top: 248px;left: calc(50% - 320px + 328px);}}@media screen and (max-width: 639px){#rec36940315 .tn-elem[data-elem-id="1509617440890"]{top: 208px;left: calc(50% - 240px + 278px);}}@media screen and (max-width: 479px){#rec36940315 .tn-elem[data-elem-id="1509617440890"]{top: 508px;left: calc(50% - 160px + 78px);}}#rec36940315 .tn-elem[data-elem-id="1510047209127"]{color:#ffffff;text-align:center;z-index:5;top: 210px;left: calc(50% - 600px + 390px);width:160px;}#rec36940315 .tn-elem[data-elem-id="1510047209127"] .tn-atom{color:#ffffff;font-size:58px;font-family:'Intro';line-height:1.55;font-weight:400;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec36940315 .tn-elem[data-elem-id="1510047209127"]{top: 160px;left: calc(50% - 480px + 350px);width:140px;}#rec36940315 .tn-elem[data-elem-id="1510047209127"]{color:#ffffff;text-align:center;}#rec36940315 .tn-elem[data-elem-id="1510047209127"] .tn-atom{color:#ffffff;font-size:45px;}}@media screen and (max-width: 959px){#rec36940315 .tn-elem[data-elem-id="1510047209127"]{top: 150px;left: calc(50% - 320px + 170px);}}@media screen and (max-width: 639px){#rec36940315 .tn-elem[data-elem-id="1510047209127"]{top: 120px;left: calc(50% - 240px + 100px);}}@media screen and (max-width: 479px){#rec36940315 .tn-elem[data-elem-id="1510047209127"]{top: 80px;left: calc(50% - 160px + 70px);}#rec36940315 .tn-elem[data-elem-id="1510047209127"] .tn-atom{font-size:30px;}}#rec36940315 .tn-elem[data-elem-id="1510047350562"]{color:#ffffff;text-align:center;z-index:6;top: 170px;left: calc(50% - 600px + 430px);width:270px;}#rec36940315 .tn-elem[data-elem-id="1510047350562"] .tn-atom{color:#ffffff;font-size:15px;font-family:'Open Sans';line-height:1.55;font-weight:600;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec36940315 .tn-elem[data-elem-id="1510047350562"]{top: 130px;left: calc(50% - 480px + 360px);}#rec36940315 .tn-elem[data-elem-id="1510047350562"]{color:#ffffff;text-align:center;}#rec36940315 .tn-elem[data-elem-id="1510047350562"] .tn-atom{color:#ffffff;font-size:14px;}}@media screen and (max-width: 959px){#rec36940315 .tn-elem[data-elem-id="1510047350562"]{top: 130px;left: calc(50% - 320px + 40px);width:550px;}#rec36940315 .tn-elem[data-elem-id="1510047350562"] .tn-atom{font-size:13px;line-height:1.55;}}@media screen and (max-width: 639px){#rec36940315 .tn-elem[data-elem-id="1510047350562"]{top: 100px;left: calc(50% - 240px + -30px);}}@media screen and (max-width: 479px){#rec36940315 .tn-elem[data-elem-id="1510047350562"]{top: 50px;left: calc(50% - 160px + -110px);}}</style>  
  
  
  

<div class='t396'>

	<div class="t396__artboard" data-artboard-recid="36940315"
		data-artboard-height="700"
		data-artboard-height-res-960="680"		data-artboard-height-res-640="680"		data-artboard-height-res-480="570"		data-artboard-height-res-320="900"
		data-artboard-height_vh=""
										
		data-artboard-valign="center"
										
		data-artboard-ovrflw=""
	>
    
      	  <div class="t396__carrier" data-artboard-recid="36940315"></div>      
            
      <div class="t396__filter" data-artboard-recid="36940315"></div>
    
	                        
	  <div class='t396__elem tn-elem tn-elem__369403151509617409307' data-elem-id='1509617409307' data-elem-type='image'
		data-field-top-value="451"
		data-field-top-res-960-value="251"		data-field-top-res-640-value="250"		data-field-top-res-480-value="210"		data-field-top-res-320-value="160"			
			
		data-field-left-value="150"
		data-field-left-res-960-value="270"		data-field-left-res-640-value="140"		data-field-left-res-480-value="50"		data-field-left-res-320-value="80"			

													

		data-field-width-value="155"
		data-field-width-res-960-value="155"		data-field-width-res-640-value="155"							

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                  
        
          <a class='tn-atom' href="#"  >
                        <img class='tn-atom__img' src='https://static.tildacdn.com/tild3434-6562-4064-b737-643966353833/2.svg'  imgfield='tn_img_1509617409307'>
                      </a>
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__369403151509617420525' data-elem-id='1509617420525' data-elem-type='image'
		data-field-top-value="416"
		data-field-top-res-960-value="410"		data-field-top-res-640-value="410"		data-field-top-res-480-value="370"		data-field-top-res-320-value="310"			
			
		data-field-left-value="390"
		data-field-left-res-960-value="270"		data-field-left-res-640-value="140"		data-field-left-res-480-value="50"		data-field-left-res-320-value="80"			

													

		data-field-width-value="145"
				data-field-width-res-640-value="145"							

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                  
        
          <a class='tn-atom' href="#"  >
                        <img class='tn-atom__img' src='https://static.tildacdn.com/tild3534-3834-4266-b137-646433356334/2.svg'  imgfield='tn_img_1509617420525'>
                      </a>
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__369403151509617431460' data-elem-id='1509617431460' data-elem-type='image'
		data-field-top-value="416"
		data-field-top-res-960-value="418"		data-field-top-res-640-value="418"		data-field-top-res-480-value="378"		data-field-top-res-320-value="678"			
			
		data-field-left-value="630"
		data-field-left-res-960-value="530"		data-field-left-res-640-value="339"		data-field-left-res-480-value="279"		data-field-left-res-320-value="85"			

													

		data-field-width-value="155"
		data-field-width-res-960-value="150"									

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                  
        
          <a class='tn-atom' href="#"  >
                        <img class='tn-atom__img' src='https://static.tildacdn.com/tild6335-3762-4436-b165-653066313736/2.svg'  imgfield='tn_img_1509617431460'>
                      </a>
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__369403151509617440890' data-elem-id='1509617440890' data-elem-type='image'
		data-field-top-value="448"
		data-field-top-res-960-value="248"		data-field-top-res-640-value="248"		data-field-top-res-480-value="208"		data-field-top-res-320-value="508"			
			
		data-field-left-value="878"
		data-field-left-res-960-value="518"		data-field-left-res-640-value="328"		data-field-left-res-480-value="278"		data-field-left-res-320-value="78"			

													

		data-field-width-value="153"
											

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                  
        
          <a class='tn-atom' href="#"  >
                        <img class='tn-atom__img' src='https://static.tildacdn.com/tild3231-6332-4565-a465-313630376465/2.svg'  imgfield='tn_img_1509617440890'>
                      </a>
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__369403151510047209127' data-elem-id='1510047209127' data-elem-type='text'
		data-field-top-value="210"
		data-field-top-res-960-value="160"		data-field-top-res-640-value="150"		data-field-top-res-480-value="120"		data-field-top-res-320-value="80"			
			
		data-field-left-value="390"
		data-field-left-res-960-value="350"		data-field-left-res-640-value="170"		data-field-left-res-480-value="100"		data-field-left-res-320-value="70"			

													

		data-field-width-value="160"
		data-field-width-res-960-value="140"									

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
            
        
          <h1 class='tn-atom'   ><span style="font-weight: 700;"><strong>ЛЕКТОРИУМ</strong></span></h1>
        
                
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__369403151510047350562' data-elem-id='1510047350562' data-elem-type='text'
		data-field-top-value="170"
		data-field-top-res-960-value="130"		data-field-top-res-640-value="130"		data-field-top-res-480-value="100"		data-field-top-res-320-value="50"			
			
		data-field-left-value="430"
		data-field-left-res-960-value="360"		data-field-left-res-640-value="40"		data-field-left-res-480-value="-30"		data-field-left-res-320-value="-110"			

													

		data-field-width-value="270"
				data-field-width-res-640-value="550"							

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
            
        
          <div class='tn-atom' field='tn_text_1510047350562'  ><span style="font-weight: 600;">Просветительский проект</span></div>
        
                
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                                                                                                                                                                                                                                                
    </div>
  
</div>  


<script>
$( document ).ready(function() {  
  t396_init('36940315');  
});
</script>



<!-- /T396 -->
</div>


<div id="rec37108276" class="r t-rec" style=" " data-animationappear="off" data-record-type="303"   >

<div class="t300" data-tooltip-hook="https://www.lektorium.tv/mooc"  data-tooltip-id="37108276" data-tooltip-position="">
  <div class="t300__content">
        <div class="t300__content-title"><div style="text-align:center;font-family:'Open Sans';" data-customstyle="yes"></div></div>    <div class="t300__content-text"><div style="text-align:center;font-family:'Open Sans';" data-customstyle="yes">Короткие студийные ролики, увлекательные задания и общение с сокурсниками и преподавателем</div></div>  </div>
</div>
  

<style type="text/css">
  .t300__tooltipster-noir_37108276 {
    background-color: #383838 !important;
    color: #fff !important;
    border-radius: 0 !important;
    ;
  }
</style>

</div>


<div id="rec36929261" class="r t-rec" style=" " data-animationappear="off" data-record-type="303"   >

<div class="t300" data-tooltip-hook="https://www.lektorium.tv/medialibrary"  data-tooltip-id="36929261" data-tooltip-position="">
  <div class="t300__content">
        <div class="t300__content-title"><div style="text-align:center;font-family:'Open Sans';" data-customstyle="yes"></div></div>    <div class="t300__content-text"><div style="text-align:center;font-family:'Open Sans';" data-customstyle="yes">Бесплатные лекции ведущих вузов, обновления каждую неделю</div></div>  </div>
</div>
  

<style type="text/css">
  .t300__tooltipster-noir_36929261 {
    background-color: #383838 !important;
    color: #fff !important;
    border-radius: 0 !important;
    ;
  }
</style>

</div>


<div id="rec36929839" class="r t-rec" style=" " data-animationappear="off" data-record-type="303"   >

<div class="t300" data-tooltip-hook="http://project.lektorium.tv/mooc-production"  data-tooltip-id="36929839" data-tooltip-position="">
  <div class="t300__content">
        <div class="t300__content-title"><div style="text-align:center;font-family:'Open Sans';" data-customstyle="yes"></div></div>    <div class="t300__content-text"><div style="text-align:center;font-family:'Open Sans';" data-customstyle="yes">Делаем онлайн-курсы от фирстиля до последней правки</div></div>  </div>
</div>
  

<style type="text/css">
  .t300__tooltipster-noir_36929839 {
    background-color: #383838 !important;
    color: #fff !important;
    border-radius: 0 !important;
    ;
  }
</style>

</div>


<div id="rec36929996" class="r t-rec" style=" " data-animationappear="off" data-record-type="303"   >

<div class="t300" data-tooltip-hook="http://project.lektorium.tv"  data-tooltip-id="36929996" data-tooltip-position="">
  <div class="t300__content">
        <div class="t300__content-title"><div style="text-align:center;font-family:'Open Sans';" data-customstyle="yes"></div></div>    <div class="t300__content-text"><div style="text-align:center;font-family:'Open Sans';" data-customstyle="yes">Особенные курсы или целые подборки от нас и наших партнеров</div></div>  </div>
</div>
  

<style type="text/css">
  .t300__tooltipster-noir_36929996 {
    background-color: #383838 !important;
    color: #fff !important;
    border-radius: 0 !important;
    ;
  }
</style>

</div>


<div id="rec42443005" class="r t-rec t-rec_pt_75 t-rec_pb_30" style="padding-top:75px;padding-bottom:30px;background-color:#ffffff; " data-animationappear="off" data-record-type="33"   data-bg-color="#ffffff">
<!-- T017 -->
<div class="t017">
  <div class="t-container t-align_center">
    <div class="t-col t-col_10 t-prefix_1">      
            <h1 class="t017__title t-title t-title_xxs" field="title" style=""><div style="font-size:38px;font-family:'Intro';color:#000000;" data-customstyle="yes">оТКРЫТИЕ КУРСА<br /></div></h1>          </div>
  </div>
</div>
</div>


<div id="rec105245640" class="r t-rec t-rec_pt_0 t-rec_pb_0" style="padding-top:0px;padding-bottom:0px; " data-animationappear="off" data-record-type="396"   >
<!-- T396 -->
<style>#rec105245640 .t396__artboard{height: 750px;background-color: #ffffff;}#rec105245640 .t396__filter{height: 750px;}#rec105245640 .t396__carrier{height: 750px;background-position: center center;background-attachment: scroll;background-size:cover;background-repeat:no-repeat;}@media screen and (max-width: 1199px){#rec105245640 .t396__artboard{height: 600px;}#rec105245640 .t396__filter{height: 600px;}#rec105245640 .t396__carrier{height: 600px;background-attachment:scroll;}}@media screen and (max-width: 959px){#rec105245640 .t396__artboard{height: 500px;}#rec105245640 .t396__filter{height: 500px;}#rec105245640 .t396__carrier{height: 500px;}}@media screen and (max-width: 639px){#rec105245640 .t396__artboard{height: 430px;background-color: #ffffff;}#rec105245640 .t396__filter{height: 430px;}#rec105245640 .t396__carrier{height: 430px;background-position: center center;}}@media screen and (max-width: 479px){#rec105245640 .t396__artboard{height: 380px;}#rec105245640 .t396__filter{height: 380px;}#rec105245640 .t396__carrier{height: 380px;}}#rec105245640 .tn-elem[data-elem-id="1470209944682"]{color:#000000;z-index:2;top: 108px;left: calc(50% - 600px + 720px);width:460px;}#rec105245640 .tn-elem[data-elem-id="1470209944682"] .tn-atom{color:#000000;font-size:42px;font-family:'Arial';line-height:1.2;font-weight:700;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec105245640 .tn-elem[data-elem-id="1470209944682"]{top: 272px;left: calc(50% - 480px + 570px);width:380px;}#rec105245640 .tn-elem[data-elem-id="1470209944682"] .tn-atom{font-size:38px;}}@media screen and (max-width: 959px){#rec105245640 .tn-elem[data-elem-id="1470209944682"]{top: 814px;left: calc(50% - 320px + 20px);width:600px;}}@media screen and (max-width: 639px){#rec105245640 .tn-elem[data-elem-id="1470209944682"]{top: 681px;left: calc(50% - 240px + 10px);width:460px;}#rec105245640 .tn-elem[data-elem-id="1470209944682"] .tn-atom{font-size:30px;}}@media screen and (max-width: 479px){#rec105245640 .tn-elem[data-elem-id="1470209944682"]{top: 487px;left: calc(50% - 160px + 10px);width:300px;}}#rec105245640 .tn-elem[data-elem-id="1470210011265"]{color:#5e5e5e;z-index:3;top: 40px;left: calc(50% - 600px + 540px);width:510px;}#rec105245640 .tn-elem[data-elem-id="1470210011265"] .tn-atom{color:#5e5e5e;font-size:12px;font-family:'Open Sans';line-height:1.5;font-weight:300;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec105245640 .tn-elem[data-elem-id="1470210011265"]{top: 28px;left: calc(50% - 480px + 455px);width:350px;}#rec105245640 .tn-elem[data-elem-id="1470210011265"] .tn-atom{font-size:10px;}}@media screen and (max-width: 959px){#rec105245640 .tn-elem[data-elem-id="1470210011265"]{top: 17px;left: calc(50% - 320px + 280px);width:310px;}#rec105245640 .tn-elem[data-elem-id="1470210011265"] .tn-atom{font-size:10px;}}@media screen and (max-width: 639px){#rec105245640 .tn-elem[data-elem-id="1470210011265"]{top: 30px;left: calc(50% - 240px + 40px);width:410px;}#rec105245640 .tn-elem[data-elem-id="1470210011265"] .tn-atom{font-size:10px;line-height:1.45;}}@media screen and (max-width: 479px){#rec105245640 .tn-elem[data-elem-id="1470210011265"]{top: 8px;left: calc(50% - 160px + 10px);width:310px;}#rec105245640 .tn-elem[data-elem-id="1470210011265"]{text-align:left;}#rec105245640 .tn-elem[data-elem-id="1470210011265"] .tn-atom{font-size:10px;}}#rec105245640 .tn-elem[data-elem-id="1513780275768"]{z-index:7;top: 511px;left: calc(50% - 600px + 180px);width:310px;}#rec105245640 .tn-elem[data-elem-id="1513780275768"] .tn-atom{background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec105245640 .tn-elem[data-elem-id="1513780275768"]{top: 410px;left: calc(50% - 480px + 150px);width:250px;}}@media screen and (max-width: 959px){#rec105245640 .tn-elem[data-elem-id="1513780275768"]{top: 330px;left: calc(50% - 320px + 60px);width:210px;}}@media screen and (max-width: 639px){#rec105245640 .tn-elem[data-elem-id="1513780275768"]{top: 300px;left: calc(50% - 240px + 40px);width:160px;}}@media screen and (max-width: 479px){#rec105245640 .tn-elem[data-elem-id="1513780275768"]{top: 294px;left: calc(50% - 160px + 10px);width:130px;}}#rec105245640 .tn-elem[data-elem-id="1513780293144"]{z-index:4;top: 119px;left: calc(50% - 600px + 170px);width:340px;}#rec105245640 .tn-elem[data-elem-id="1513780293144"] .tn-atom{background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec105245640 .tn-elem[data-elem-id="1513780293144"]{top: 78px;left: calc(50% - 480px + 120px);width:310px;}}@media screen and (max-width: 959px){#rec105245640 .tn-elem[data-elem-id="1513780293144"]{top: 98px;left: calc(50% - 320px + 70px);width:190px;}}@media screen and (max-width: 639px){#rec105245640 .tn-elem[data-elem-id="1513780293144"]{top: 108px;left: calc(50% - 240px + 40px);width:160px;}}@media screen and (max-width: 479px){#rec105245640 .tn-elem[data-elem-id="1513780293144"]{top: 145px;left: calc(50% - 160px + 10px);width:140px;}}#rec105245640 .tn-elem[data-elem-id="1513780309296"]{z-index:5;top: 230px;left: calc(50% - 600px + 407px);width:680px;}#rec105245640 .tn-elem[data-elem-id="1513780309296"] .tn-atom{background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec105245640 .tn-elem[data-elem-id="1513780309296"]{top: 200px;left: calc(50% - 480px + 335px);width:430px;}}@media screen and (max-width: 959px){#rec105245640 .tn-elem[data-elem-id="1513780309296"]{top: 180px;left: calc(50% - 320px + 195px);width:310px;}}@media screen and (max-width: 639px){#rec105245640 .tn-elem[data-elem-id="1513780309296"]{top: 180px;left: calc(50% - 240px + 175px);width:270px;}}@media screen and (max-width: 479px){#rec105245640 .tn-elem[data-elem-id="1513780309296"]{top: 201px;left: calc(50% - 160px + 125px);width:190px;}}#rec105245640 .tn-elem[data-elem-id="1513780658674"]{z-index:6;top: 333px;left: calc(50% - 600px + 115px);width:280px;}#rec105245640 .tn-elem[data-elem-id="1513780658674"] .tn-atom{background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec105245640 .tn-elem[data-elem-id="1513780658674"]{top: 273px;left: calc(50% - 480px + 95px);width:230px;}}@media screen and (max-width: 959px){#rec105245640 .tn-elem[data-elem-id="1513780658674"]{top: 213px;left: calc(50% - 320px + 15px);width:170px;}}@media screen and (max-width: 639px){#rec105245640 .tn-elem[data-elem-id="1513780658674"]{top: 213px;left: calc(50% - 240px + 25px);width:130px;}}@media screen and (max-width: 479px){#rec105245640 .tn-elem[data-elem-id="1513780658674"]{top: 230px;left: calc(50% - 160px + 22px);width:100px;}}#rec105245640 .tn-elem[data-elem-id="1513849239776"]{color:#000000;z-index:8;top: -10px;left: calc(50% - 600px + 510px);width:560px;}#rec105245640 .tn-elem[data-elem-id="1513849239776"] .tn-atom{color:#000000;font-size:41px;font-family:'Open Sans';line-height:1.55;font-weight:700;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec105245640 .tn-elem[data-elem-id="1513849239776"]{top: 10px;left: calc(50% - 480px + 460px);}#rec105245640 .tn-elem[data-elem-id="1513849239776"] .tn-atom{font-size:26px;}}@media screen and (max-width: 959px){#rec105245640 .tn-elem[data-elem-id="1513849239776"]{top: 0px;left: calc(50% - 320px + 290px);width:310px;}#rec105245640 .tn-elem[data-elem-id="1513849239776"] .tn-atom{font-size:21px;}}@media screen and (max-width: 639px){#rec105245640 .tn-elem[data-elem-id="1513849239776"]{top: 10px;left: calc(50% - 240px + 210px);width:230px;}#rec105245640 .tn-elem[data-elem-id="1513849239776"] .tn-atom{font-size:16px;}}@media screen and (max-width: 479px){#rec105245640 .tn-elem[data-elem-id="1513849239776"]{top: 0px;left: calc(50% - 160px + 20px);}#rec105245640 .tn-elem[data-elem-id="1513849239776"] .tn-atom{font-size:13px;}}#rec105245640 .tn-elem[data-elem-id="1518520386752"]{color:#333333;z-index:9;top: 78px;left: calc(50% - 600px + 539px);width:560px;}#rec105245640 .tn-elem[data-elem-id="1518520386752"] .tn-atom{color:#333333;font-size:26px;font-family:'Intro';line-height:1.55;font-weight:500;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec105245640 .tn-elem[data-elem-id="1518520386752"]{top: 70px;left: calc(50% - 480px + 455px);}#rec105245640 .tn-elem[data-elem-id="1518520386752"] .tn-atom{font-size:20px;}}@media screen and (max-width: 959px){#rec105245640 .tn-elem[data-elem-id="1518520386752"]{top: 71px;left: calc(50% - 320px + 280px);width:360px;}#rec105245640 .tn-elem[data-elem-id="1518520386752"] .tn-atom{font-size:17px;}}@media screen and (max-width: 639px){#rec105245640 .tn-elem[data-elem-id="1518520386752"]{top: 70px;left: calc(50% - 240px + 40px);width:320px;}#rec105245640 .tn-elem[data-elem-id="1518520386752"] .tn-atom{font-size:16px;}}@media screen and (max-width: 479px){#rec105245640 .tn-elem[data-elem-id="1518520386752"]{top: 61px;left: calc(50% - 160px + 10px);width:280px;}#rec105245640 .tn-elem[data-elem-id="1518520386752"] .tn-atom{font-size:16px;line-height:1.2;}}#rec105245640 .tn-elem[data-elem-id="1518520554503"]{color:#3b3b3b;z-index:10;top: 115px;left: calc(50% - 600px + 540px);width:520px;}#rec105245640 .tn-elem[data-elem-id="1518520554503"] .tn-atom{color:#3b3b3b;font-size:15px;font-family:'Open Sans';line-height:1.55;font-weight:400;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec105245640 .tn-elem[data-elem-id="1518520554503"]{top: 100px;left: calc(50% - 480px + 455px);width:369px;}#rec105245640 .tn-elem[data-elem-id="1518520554503"] .tn-atom{font-size:12px;}}@media screen and (max-width: 959px){#rec105245640 .tn-elem[data-elem-id="1518520554503"]{top: 97px;left: calc(50% - 320px + 280px);}#rec105245640 .tn-elem[data-elem-id="1518520554503"] .tn-atom{font-size:12px;}}@media screen and (max-width: 639px){#rec105245640 .tn-elem[data-elem-id="1518520554503"]{top: 103px;left: calc(50% - 240px + 210px);width:250px;}#rec105245640 .tn-elem[data-elem-id="1518520554503"] .tn-atom{font-size:11px;}}@media screen and (max-width: 479px){#rec105245640 .tn-elem[data-elem-id="1518520554503"]{top: 106px;left: calc(50% - 160px + 10px);width:290px;}#rec105245640 .tn-elem[data-elem-id="1518520554503"] .tn-atom{font-size:12px;line-height:1.15;}}#rec105245640 .tn-elem[data-elem-id="1518520767262"]{color:#ffcd7f;z-index:11;top: 180px;left: calc(50% - 600px + 540px);width:560px;}#rec105245640 .tn-elem[data-elem-id="1518520767262"] .tn-atom{color:#ffcd7f;font-size:15px;font-family:'Open Sans';line-height:1.55;font-weight:600;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec105245640 .tn-elem[data-elem-id="1518520767262"]{top: 160px;left: calc(50% - 480px + 455px);}}@media screen and (max-width: 959px){#rec105245640 .tn-elem[data-elem-id="1518520767262"]{top: 140px;left: calc(50% - 320px + 280px);}#rec105245640 .tn-elem[data-elem-id="1518520767262"] .tn-atom{font-size:11px;}}@media screen and (max-width: 639px){#rec105245640 .tn-elem[data-elem-id="1518520767262"]{top: 155px;left: calc(50% - 240px + 210px);}}@media screen and (max-width: 479px){#rec105245640 .tn-elem[data-elem-id="1518520767262"]{top: 145px;left: calc(50% - 160px + 160px);width:110px;}}#rec105245640 .tn-elem[data-elem-id="1518521113627"]{color:#000000;text-align:center;z-index:12;top: 660px;left: calc(50% - 600px + 540px);width:160px;height:54px;}#rec105245640 .tn-elem[data-elem-id="1518521113627"] .tn-atom{color:#000000;font-size:16px;font-family:'Intro';line-height:1.55;font-weight:600;border-width:1px;border-radius:0px;background-color:#f7f7f7;background-position:center center;border-color:#000000;border-style:solid;transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out, border-color 0.2s ease-in-out;}#rec105245640 .tn-elem[data-elem-id="1518521113627"] .tn-atom:hover{background-color:#FFCD7F;color:#ffffff;}@media screen and (max-width: 1199px){#rec105245640 .tn-elem[data-elem-id="1518521113627"]{top: 470px;left: calc(50% - 480px + 455px);}}@media screen and (max-width: 959px){#rec105245640 .tn-elem[data-elem-id="1518521113627"]{top: 380px;left: calc(50% - 320px + 280px);width:90px;height:34px;}#rec105245640 .tn-elem[data-elem-id="1518521113627"] .tn-atom{font-size:12px;}}@media screen and (max-width: 639px){#rec105245640 .tn-elem[data-elem-id="1518521113627"]{top: 350px;left: calc(50% - 240px + 210px);width:60px;height:24px;}#rec105245640 .tn-elem[data-elem-id="1518521113627"] .tn-atom{font-size:10px;}}@media screen and (max-width: 479px){#rec105245640 .tn-elem[data-elem-id="1518521113627"]{top: 316px;left: calc(50% - 160px + 161px);width:70px;height:28px;}}</style>  
  
  
  

<div class='t396'>

	<div class="t396__artboard" data-artboard-recid="105245640"
		data-artboard-height="750"
		data-artboard-height-res-960="600"		data-artboard-height-res-640="500"		data-artboard-height-res-480="430"		data-artboard-height-res-320="380"
		data-artboard-height_vh=""
										
		data-artboard-valign="center"
										
		data-artboard-ovrflw=""
	>
    
      	  <div class="t396__carrier" data-artboard-recid="105245640"></div>      
            
      <div class="t396__filter" data-artboard-recid="105245640"></div>
    
	                        
	  <div class='t396__elem tn-elem tn-elem__1052456401470209944682' data-elem-id='1470209944682' data-elem-type='text'
		data-field-top-value="108"
		data-field-top-res-960-value="272"		data-field-top-res-640-value="814"		data-field-top-res-480-value="681"		data-field-top-res-320-value="487"			
			
		data-field-left-value="720"
		data-field-left-res-960-value="570"		data-field-left-res-640-value="20"		data-field-left-res-480-value="10"		data-field-left-res-320-value="10"			

													

		data-field-width-value="460"
		data-field-width-res-960-value="380"		data-field-width-res-640-value="600"		data-field-width-res-480-value="460"		data-field-width-res-320-value="300"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
            
        
          <div class='tn-atom' field='tn_text_1470209944682'  ></div>
        
                
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1052456401470210011265' data-elem-id='1470210011265' data-elem-type='text'
		data-field-top-value="40"
		data-field-top-res-960-value="28"		data-field-top-res-640-value="17"		data-field-top-res-480-value="30"		data-field-top-res-320-value="8"			
			
		data-field-left-value="540"
		data-field-left-res-960-value="455"		data-field-left-res-640-value="280"		data-field-left-res-480-value="40"		data-field-left-res-320-value="10"			

													

		data-field-width-value="510"
		data-field-width-res-960-value="350"		data-field-width-res-640-value="310"		data-field-width-res-480-value="410"		data-field-width-res-320-value="310"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
            
        
          <div class='tn-atom' field='tn_text_1470210011265'  >Курс Академии наставников совместно с&nbsp;Кружковым движением НТИ и&nbsp;Фондом Сколково при поддержке Агентства стратегических инициатив</div>
        
                
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1052456401513780275768' data-elem-id='1513780275768' data-elem-type='image'
		data-field-top-value="511"
		data-field-top-res-960-value="410"		data-field-top-res-640-value="330"		data-field-top-res-480-value="300"		data-field-top-res-320-value="294"			
			
		data-field-left-value="180"
		data-field-left-res-960-value="150"		data-field-left-res-640-value="60"		data-field-left-res-480-value="40"		data-field-left-res-320-value="10"			

													

		data-field-width-value="310"
		data-field-width-res-960-value="250"		data-field-width-res-640-value="210"		data-field-width-res-480-value="160"		data-field-width-res-320-value="130"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                  
        
          <div class='tn-atom'   data-zoomable="yes" data-img-zoom-url="https://static.tildacdn.com/tild6565-3261-4038-b130-356333613237/2.png">
                        <img class='tn-atom__img' src='https://static.tildacdn.com/tild6565-3261-4038-b130-356333613237/2.png'  imgfield='tn_img_1513780275768'>
                      </div>
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1052456401513780293144' data-elem-id='1513780293144' data-elem-type='image'
		data-field-top-value="119"
		data-field-top-res-960-value="78"		data-field-top-res-640-value="98"		data-field-top-res-480-value="108"		data-field-top-res-320-value="145"			
			
		data-field-left-value="170"
		data-field-left-res-960-value="120"		data-field-left-res-640-value="70"		data-field-left-res-480-value="40"		data-field-left-res-320-value="10"			

													

		data-field-width-value="340"
		data-field-width-res-960-value="310"		data-field-width-res-640-value="190"		data-field-width-res-480-value="160"		data-field-width-res-320-value="140"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                  
        
          <div class='tn-atom'   data-zoomable="yes" data-img-zoom-url="https://static.tildacdn.com/tild6139-3337-4364-b461-353139616132/4.png">
                        <img class='tn-atom__img' src='https://static.tildacdn.com/tild6139-3337-4364-b461-353139616132/4.png'  imgfield='tn_img_1513780293144'>
                      </div>
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1052456401513780309296' data-elem-id='1513780309296' data-elem-type='image'
		data-field-top-value="230"
		data-field-top-res-960-value="200"		data-field-top-res-640-value="180"		data-field-top-res-480-value="180"		data-field-top-res-320-value="201"			
			
		data-field-left-value="407"
		data-field-left-res-960-value="335"		data-field-left-res-640-value="195"		data-field-left-res-480-value="175"		data-field-left-res-320-value="125"			

													

		data-field-width-value="680"
		data-field-width-res-960-value="430"		data-field-width-res-640-value="310"		data-field-width-res-480-value="270"		data-field-width-res-320-value="190"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                  
        
          <div class='tn-atom'   data-zoomable="yes" data-img-zoom-url="https://static.tildacdn.com/tild3337-6238-4365-a133-363033316664/11.png">
                        <img class='tn-atom__img' src='https://static.tildacdn.com/tild3337-6238-4365-a133-363033316664/11.png'  imgfield='tn_img_1513780309296'>
                      </div>
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1052456401513780658674' data-elem-id='1513780658674' data-elem-type='image'
		data-field-top-value="333"
		data-field-top-res-960-value="273"		data-field-top-res-640-value="213"		data-field-top-res-480-value="213"		data-field-top-res-320-value="230"			
			
		data-field-left-value="115"
		data-field-left-res-960-value="95"		data-field-left-res-640-value="15"		data-field-left-res-480-value="25"		data-field-left-res-320-value="22"			

													

		data-field-width-value="280"
		data-field-width-res-960-value="230"		data-field-width-res-640-value="170"		data-field-width-res-480-value="130"		data-field-width-res-320-value="100"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                  
        
          <div class='tn-atom'   data-zoomable="yes" data-img-zoom-url="https://static.tildacdn.com/tild6233-6333-4937-a463-306432303938/3.png">
                        <img class='tn-atom__img' src='https://static.tildacdn.com/tild6233-6333-4937-a463-306432303938/3.png'  imgfield='tn_img_1513780658674'>
                      </div>
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1052456401513849239776' data-elem-id='1513849239776' data-elem-type='text'
		data-field-top-value="-10"
		data-field-top-res-960-value="10"		data-field-top-res-640-value="0"		data-field-top-res-480-value="10"		data-field-top-res-320-value="0"			
			
		data-field-left-value="510"
		data-field-left-res-960-value="460"		data-field-left-res-640-value="290"		data-field-left-res-480-value="210"		data-field-left-res-320-value="20"			

													

		data-field-width-value="560"
				data-field-width-res-640-value="310"		data-field-width-res-480-value="230"					

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
            
        
          <div class='tn-atom' field='tn_text_1513849239776'  ></div>
        
                
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1052456401518520386752' data-elem-id='1518520386752' data-elem-type='text'
		data-field-top-value="78"
		data-field-top-res-960-value="70"		data-field-top-res-640-value="71"		data-field-top-res-480-value="70"		data-field-top-res-320-value="61"			
			
		data-field-left-value="539"
		data-field-left-res-960-value="455"		data-field-left-res-640-value="280"		data-field-left-res-480-value="40"		data-field-left-res-320-value="10"			

													

		data-field-width-value="560"
				data-field-width-res-640-value="360"		data-field-width-res-480-value="320"		data-field-width-res-320-value="280"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
            
        
          <div class='tn-atom' field='tn_text_1518520386752'  >КАК СТАТЬ НАСТАВНИКОМ ПРОЕКТОВ</div>
        
                
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1052456401518520554503' data-elem-id='1518520554503' data-elem-type='text'
		data-field-top-value="115"
		data-field-top-res-960-value="100"		data-field-top-res-640-value="97"		data-field-top-res-480-value="103"		data-field-top-res-320-value="106"			
			
		data-field-left-value="540"
		data-field-left-res-960-value="455"		data-field-left-res-640-value="280"		data-field-left-res-480-value="210"		data-field-left-res-320-value="10"			

													

		data-field-width-value="520"
		data-field-width-res-960-value="369"				data-field-width-res-480-value="250"		data-field-width-res-320-value="290"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
            
        
          <div class='tn-atom' field='tn_text_1518520554503'  >Для всех, кто хочет отточить мастерство создания школьных и студенческих проектов.</div>
        
                
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1052456401518520767262' data-elem-id='1518520767262' data-elem-type='text'
		data-field-top-value="180"
		data-field-top-res-960-value="160"		data-field-top-res-640-value="140"		data-field-top-res-480-value="155"		data-field-top-res-320-value="145"			
			
		data-field-left-value="540"
		data-field-left-res-960-value="455"		data-field-left-res-640-value="280"		data-field-left-res-480-value="210"		data-field-left-res-320-value="160"			

													

		data-field-width-value="560"
								data-field-width-res-320-value="110"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
            
        
          <div class='tn-atom' field='tn_text_1518520767262'  >#КурсНаставников</div>
        
                
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1052456401518521113627' data-elem-id='1518521113627' data-elem-type='button'
		data-field-top-value="660"
		data-field-top-res-960-value="470"		data-field-top-res-640-value="380"		data-field-top-res-480-value="350"		data-field-top-res-320-value="316"			
			
		data-field-left-value="540"
		data-field-left-res-960-value="455"		data-field-left-res-640-value="280"		data-field-left-res-480-value="210"		data-field-left-res-320-value="161"			

		data-field-height-value="54"				data-field-height-res-640-value="34"		data-field-height-res-480-value="24"		data-field-height-res-320-value="28"			

		data-field-width-value="160"
				data-field-width-res-640-value="90"		data-field-width-res-480-value="60"		data-field-width-res-320-value="70"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                
        
             
        
        
                  
        
          <a class='tn-atom' href="#" >К КУРСУ</a>
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                                                                                                                                                                          
    </div>
  
</div>  


<script>
$( document ).ready(function() {  
  t396_init('105245640');  
});
</script>



<!-- /T396 -->
</div>


<div id="rec38915347" class="r t-rec t-rec_pt_105 t-rec_pb_30" style="padding-top:105px;padding-bottom:30px; "  data-record-type="33"   >
<!-- T017 -->
<div class="t017">
  <div class="t-container t-align_center">
    <div class="t-col t-col_10 t-prefix_1">      
            <div class="t017__title t-title t-title_xxs" field="title" style=""><div style="font-size:38px;" data-customstyle="yes">курсы, которые Cкоро начнутся</div></div>          </div>
  </div>
</div>
</div>


<div id="rec103214131" class="r t-rec t-rec_pb_30" style="padding-bottom:30px; " data-animationappear="off" data-record-type="396"   >
<!-- T396 -->
<style>#rec103214131 .t396__artboard{height: 275px;background-color: #ffffff;}#rec103214131 .t396__filter{height: 275px;}#rec103214131 .t396__carrier{height: 275px;background-position: center center;background-attachment: scroll;background-size:cover;background-repeat:no-repeat;}@media screen and (max-width: 1199px){#rec103214131 .t396__artboard{height: 205px;background-color: #ffffff;}#rec103214131 .t396__filter{height: 205px;}#rec103214131 .t396__carrier{height: 205px;background-position: center center;background-attachment:scroll;}}@media screen and (max-width: 959px){#rec103214131 .t396__artboard{height: 475px;}#rec103214131 .t396__filter{height: 475px;}#rec103214131 .t396__carrier{height: 475px;}}@media screen and (max-width: 639px){#rec103214131 .t396__artboard{height: 350px;}#rec103214131 .t396__filter{height: 350px;}#rec103214131 .t396__carrier{height: 350px;}}@media screen and (max-width: 479px){#rec103214131 .t396__artboard{height: 630px;background-color: #ffffff;}#rec103214131 .t396__filter{height: 630px;}#rec103214131 .t396__carrier{height: 630px;background-position: center center;}}#rec103214131 .tn-elem[data-elem-id="1510752425003"]{color:#ffffff;text-align:center;z-index:22;top: 90px;left: calc(50% - 600px + 958px);width:300px;}#rec103214131 .tn-elem[data-elem-id="1510752425003"] .tn-atom{color:#ffffff;font-size:20px;font-family:'Open Sans';line-height:1;font-weight:600;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1510752425003"]{top: 87px;left: calc(50% - 480px + 738px);width:210px;}#rec103214131 .tn-elem[data-elem-id="1510752425003"] .tn-atom{font-size:14px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1510752425003"]{top: 337px;left: calc(50% - 320px + 328px);width:290px;}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1510752425003"]{top: 235px;left: calc(50% - 240px + 257px);width:220px;}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1510752425003"]{top: 517px;left: calc(50% - 160px + 46px);width:230px;}}#rec103214131 .tn-elem[data-elem-id="1558429982717"]{z-index:212;top: 8px;left: calc(50% - 600px + -68px);width:325px;height:252px;}#rec103214131 .tn-elem[data-elem-id="1558429982717"] .tn-atom{background-color:#c9c9c9;background-position:center center;background-size:cover;background-repeat:no-repeat;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1558429982717"]{top: 20px;left: calc(50% - 480px + 10px);width:220px;height:165px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1558429982717"]{top: 10px;left: calc(50% - 320px + 20px);width:295px;height:222px;}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1558429982717"]{top: 10px;left: calc(50% - 240px + 10px);width:225px;height:160px;}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1558429982717"]{top: 11px;left: calc(50% - 160px + 42px);width:241px;height:145px;}}#rec103214131 .tn-elem[data-elem-id="1558429982739"]{color:#ffffff;text-align:center;z-index:214;top: 74px;left: calc(50% - 600px + -60px);width:305px;}#rec103214131 .tn-elem[data-elem-id="1558429982739"] .tn-atom{color:#ffffff;font-size:20px;font-family:'Open Sans';line-height:1;font-weight:600;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1558429982739"]{top: 74px;left: calc(50% - 480px + 10px);width:215px;}#rec103214131 .tn-elem[data-elem-id="1558429982739"] .tn-atom{font-size:14px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1558429982739"]{top: 95px;left: calc(50% - 320px + 30px);width:275px;}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1558429982739"]{top: 60px;left: calc(50% - 240px + 10px);width:215px;}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1558429982739"]{top: 53px;left: calc(50% - 160px + 50px);width:225px;}#rec103214131 .tn-elem[data-elem-id="1558429982739"] .tn-atom{font-size:14px;}}#rec103214131 .tn-elem[data-elem-id="1558429982747"]{color:#ffffff;text-align:center;z-index:215;top: 153px;left: calc(50% - 600px + -60px);width:310px;}#rec103214131 .tn-elem[data-elem-id="1558429982747"] .tn-atom{color:#ffffff;font-size:11px;font-family:'Open Sans';line-height:1.55;font-weight:500;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1558429982747"]{top: 120px;left: calc(50% - 480px + 20px);width:200px;}#rec103214131 .tn-elem[data-elem-id="1558429982747"] .tn-atom{font-size:11px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1558429982747"]{top: 130px;left: calc(50% - 320px + 20px);width:290px;}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1558429982747"]{top: 105px;left: calc(50% - 240px + 12px);width:210px;}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1558429982747"]{top: 94px;left: calc(50% - 160px + 50px);width:220px;}}#rec103214131 .tn-elem[data-elem-id="1558429982753"]{z-index:213;top: 12px;left: calc(50% - 600px + -64px);width:317px;height:243px;}#rec103214131 .tn-elem[data-elem-id="1558429982753"] .tn-atom{opacity:0.3;background-color:#000000;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1558429982753"]{top: 22px;left: calc(50% - 480px + 12px);width:216px;height:161px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1558429982753"]{top: 12px;left: calc(50% - 320px + 22px);width:291px;height:218px;}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1558429982753"]{top: 12px;left: calc(50% - 240px + 12px);width:221px;height:156px;}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1558429982753"]{top: 13px;left: calc(50% - 160px + 44px);width:237px;height:141px;}}#rec103214131 .tn-elem[data-elem-id="1558429982762"]{color:#ffffff;text-align:center;z-index:216;top: 217px;left: calc(50% - 600px + 40px);width:110px;height:25px;}#rec103214131 .tn-elem[data-elem-id="1558429982762"] .tn-atom{color:#ffffff;font-size:11px;font-family:'Open Sans';line-height:1.55;font-weight:600;border-width:1px;opacity:0.85;background-color:#3d3d3d;background-position:center center;border-color:#000000;border-style:solid;transition: background-color 0.1s ease-in-out, color 0.1s ease-in-out, border-color 0.1s ease-in-out;}#rec103214131 .tn-elem[data-elem-id="1558429982762"] .tn-atom:hover{background-color:#ce4d46;color:#ffffff;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1558429982762"]{top: 160px;left: calc(50% - 480px + 73px);width:90px;height:17px;}#rec103214131 .tn-elem[data-elem-id="1558429982762"] .tn-atom{font-size:10px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1558429982762"]{top: 200px;left: calc(50% - 320px + 120px);}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1558429982762"]{top: 140px;left: calc(50% - 240px + 78px);}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1558429982762"]{top: 127px;left: calc(50% - 160px + 115px);}}#rec103214131 .tn-elem[data-elem-id="1558429982774"]{z-index:217;top: 217px;left: calc(50% - 600px + 210px);width:30px;}#rec103214131 .tn-elem[data-elem-id="1558429982774"] .tn-atom{opacity:0.95;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1558429982774"]{top: 158px;left: calc(50% - 480px + 200px);width:20px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1558429982774"]{top: 198px;left: calc(50% - 320px + 280px);}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1558429982774"]{top: 138px;left: calc(50% - 240px + 200px);}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1558429982774"]{top: 128px;left: calc(50% - 160px + 250px);}}#rec103214131 .tn-elem[data-elem-id="1560773370396"]{z-index:266;top: 9px;left: calc(50% - 600px + 608px);width:325px;height:252px;}#rec103214131 .tn-elem[data-elem-id="1560773370396"] .tn-atom{background-color:#c9c9c9;background-position:center center;background-size:cover;background-repeat:no-repeat;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1560773370396"]{top: 20px;left: calc(50% - 480px + 493px);width:220px;height:165px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1560773370396"]{top: 244px;left: calc(50% - 320px + 20px);width:295px;height:222px;}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1560773370396"]{top: 180px;left: calc(50% - 240px + 10px);width:225px;height:160px;}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1560773370396"]{top: 317px;left: calc(50% - 160px + 42px);width:241px;height:144px;}}#rec103214131 .tn-elem[data-elem-id="1560773370434"]{z-index:267;top: 13px;left: calc(50% - 600px + 612px);width:317px;height:243px;}#rec103214131 .tn-elem[data-elem-id="1560773370434"] .tn-atom{opacity:0.2;background-color:#000000;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1560773370434"]{top: 22px;left: calc(50% - 480px + 495px);width:216px;height:161px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1560773370434"]{top: 246px;left: calc(50% - 320px + 22px);width:291px;height:218px;}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1560773370434"]{top: 183px;left: calc(50% - 240px + 12px);width:221px;height:156px;}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1560773370434"]{top: 318px;left: calc(50% - 160px + 44px);width:237px;height:141px;}}#rec103214131 .tn-elem[data-elem-id="1560773370453"]{z-index:268;top: 218px;left: calc(50% - 600px + 888px);width:30px;}#rec103214131 .tn-elem[data-elem-id="1560773370453"] .tn-atom{opacity:0.95;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1560773370453"]{top: 158px;left: calc(50% - 480px + 200px);width:20px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1560773370453"]{top: 433px;left: calc(50% - 320px + 286px);}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1560773370453"]{top: 309px;left: calc(50% - 240px + 205px);}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1560773370453"]{top: 430px;left: calc(50% - 160px + 250px);}}#rec103214131 .tn-elem[data-elem-id="1560773370466"]{color:#ffffff;text-align:center;z-index:269;top: 218px;left: calc(50% - 600px + 718px);width:110px;height:25px;}#rec103214131 .tn-elem[data-elem-id="1560773370466"] .tn-atom{color:#ffffff;font-size:11px;font-family:'Open Sans';line-height:1.55;font-weight:600;border-width:1px;opacity:0.85;background-color:#3d3d3d;background-position:center center;border-color:#000000;border-style:solid;transition: background-color 0.1s ease-in-out, color 0.1s ease-in-out, border-color 0.1s ease-in-out;}#rec103214131 .tn-elem[data-elem-id="1560773370466"] .tn-atom:hover{background-color:#ce4d46;color:#ffffff;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1560773370466"]{top: 160px;left: calc(50% - 480px + 556px);width:90px;height:17px;}#rec103214131 .tn-elem[data-elem-id="1560773370466"] .tn-atom{font-size:10px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1560773370466"]{top: 432px;left: calc(50% - 320px + 126px);}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1560773370466"]{top: 310px;left: calc(50% - 240px + 83px);}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1560773370466"]{top: 429px;left: calc(50% - 160px + 115px);}}#rec103214131 .tn-elem[data-elem-id="1560773370477"]{color:#ffffff;text-align:center;z-index:270;top: 153px;left: calc(50% - 600px + 618px);width:310px;}#rec103214131 .tn-elem[data-elem-id="1560773370477"] .tn-atom{color:#ffffff;font-size:11px;font-family:'Open Sans';line-height:1.55;font-weight:500;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1560773370477"]{top: 120px;left: calc(50% - 480px + 503px);width:200px;}#rec103214131 .tn-elem[data-elem-id="1560773370477"] .tn-atom{font-size:11px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1560773370477"]{top: 372px;left: calc(50% - 320px + 26px);width:290px;}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1560773370477"]{top: 271px;left: calc(50% - 240px + 15px);width:220px;}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1560773370477"]{top: 398px;left: calc(50% - 160px + 40px);width:240px;}}#rec103214131 .tn-elem[data-elem-id="1560773370483"]{color:#ffffff;text-align:center;z-index:271;top: 74px;left: calc(50% - 600px + 618px);width:315px;}#rec103214131 .tn-elem[data-elem-id="1560773370483"] .tn-atom{color:#ffffff;font-size:20px;font-family:'Open Sans';line-height:1;font-weight:600;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1560773370483"]{top: 74px;left: calc(50% - 480px + 498px);width:215px;}#rec103214131 .tn-elem[data-elem-id="1560773370483"] .tn-atom{font-size:14px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1560773370483"]{top: 315px;left: calc(50% - 320px + 26px);width:285px;}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1560773370483"]{top: 225px;left: calc(50% - 240px + 15px);width:215px;}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1560773370483"]{top: 354px;left: calc(50% - 160px + 51px);width:225px;}#rec103214131 .tn-elem[data-elem-id="1560773370483"] .tn-atom{font-size:14px;}}#rec103214131 .tn-elem[data-elem-id="1560773370489"]{z-index:272;top: 9px;left: calc(50% - 600px + 948px);width:325px;height:252px;}#rec103214131 .tn-elem[data-elem-id="1560773370489"] .tn-atom{background-color:#c9c9c9;background-position:center center;background-size:cover;background-repeat:no-repeat;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1560773370489"]{top: 20px;left: calc(50% - 480px + 729px);width:220px;height:165px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1560773370489"]{top: 244px;left: calc(50% - 320px + 326px);width:295px;height:222px;}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1560773370489"]{top: 180px;left: calc(50% - 240px + 245px);width:225px;height:160px;}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1560773370489"]{top: 472px;left: calc(50% - 160px + 42px);width:241px;height:145px;}}#rec103214131 .tn-elem[data-elem-id="1560773370499"]{color:#ffffff;text-align:center;z-index:274;top: 74px;left: calc(50% - 600px + 1000px);width:225px;}#rec103214131 .tn-elem[data-elem-id="1560773370499"] .tn-atom{color:#ffffff;font-size:20px;font-family:'Open Sans';line-height:1;font-weight:600;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1560773370499"]{top: 74px;left: calc(50% - 480px + 737px);width:215px;}#rec103214131 .tn-elem[data-elem-id="1560773370499"] .tn-atom{font-size:14px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1560773370499"]{top: 314px;left: calc(50% - 320px + 336px);width:275px;}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1560773370499"]{top: 224px;left: calc(50% - 240px + 251px);width:215px;}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1560773370499"]{top: 512px;left: calc(50% - 160px + 52px);width:225px;}#rec103214131 .tn-elem[data-elem-id="1560773370499"] .tn-atom{font-size:14px;}}#rec103214131 .tn-elem[data-elem-id="1560773370505"]{color:#ffffff;text-align:center;z-index:275;top: 153px;left: calc(50% - 600px + 958px);width:310px;}#rec103214131 .tn-elem[data-elem-id="1560773370505"] .tn-atom{color:#ffffff;font-size:11px;font-family:'Open Sans';line-height:1.55;font-weight:500;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1560773370505"]{top: 120px;left: calc(50% - 480px + 747px);width:200px;}#rec103214131 .tn-elem[data-elem-id="1560773370505"] .tn-atom{font-size:11px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1560773370505"]{top: 364px;left: calc(50% - 320px + 328px);width:290px;}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1560773370505"]{top: 270px;left: calc(50% - 240px + 247px);width:220px;}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1560773370505"]{top: 557px;left: calc(50% - 160px + 50px);width:230px;}}#rec103214131 .tn-elem[data-elem-id="1560773370510"]{z-index:273;top: 13px;left: calc(50% - 600px + 952px);width:317px;height:243px;}#rec103214131 .tn-elem[data-elem-id="1560773370510"] .tn-atom{opacity:0.25;background-color:#000000;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1560773370510"]{top: 22px;left: calc(50% - 480px + 731px);width:216px;height:161px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1560773370510"]{top: 246px;left: calc(50% - 320px + 328px);width:291px;height:218px;}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1560773370510"]{top: 182px;left: calc(50% - 240px + 247px);width:221px;height:156px;}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1560773370510"]{top: 474px;left: calc(50% - 160px + 44px);width:237px;height:141px;}}#rec103214131 .tn-elem[data-elem-id="1560773370520"]{color:#ffffff;text-align:center;z-index:276;top: 218px;left: calc(50% - 600px + 1056px);width:110px;height:25px;}#rec103214131 .tn-elem[data-elem-id="1560773370520"] .tn-atom{color:#ffffff;font-size:11px;font-family:'Open Sans';line-height:1.55;font-weight:600;border-width:1px;opacity:0.85;background-color:#3d3d3d;background-position:center center;border-color:#000000;border-style:solid;transition: background-color 0.1s ease-in-out, color 0.1s ease-in-out, border-color 0.1s ease-in-out;}#rec103214131 .tn-elem[data-elem-id="1560773370520"] .tn-atom:hover{background-color:#ce4d46;color:#ffffff;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1560773370520"]{top: 158px;left: calc(50% - 480px + 793px);width:90px;height:17px;}#rec103214131 .tn-elem[data-elem-id="1560773370520"] .tn-atom{font-size:10px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1560773370520"]{top: 433px;left: calc(50% - 320px + 426px);}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1560773370520"]{top: 310px;left: calc(50% - 240px + 313px);}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1560773370520"]{top: 588px;left: calc(50% - 160px + 115px);}}#rec103214131 .tn-elem[data-elem-id="1560773458443"]{z-index:277;top: 218px;left: calc(50% - 600px + 1229px);width:30px;}#rec103214131 .tn-elem[data-elem-id="1560773458443"] .tn-atom{opacity:0.95;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1560773458443"]{top: 158px;left: calc(50% - 480px + 680px);width:20px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1560773458443"]{top: 523px;left: calc(50% - 320px + 306px);}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1560773458443"]{top: 309px;left: calc(50% - 240px + 442px);}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1560773458443"]{top: 660px;left: calc(50% - 160px + 300px);}}#rec103214131 .tn-elem[data-elem-id="1560773505001"]{z-index:278;top: 228pxpx;left: calc(50% - 600px + 1239pxpx);width:30px;}#rec103214131 .tn-elem[data-elem-id="1560773505001"] .tn-atom{opacity:0.95;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1560773505001"]{top: 158px;left: calc(50% - 480px + 920px);width:20px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1560773505001"]{top: 434px;left: calc(50% - 320px + 591px);}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1560773505001"]{top: 419px;left: calc(50% - 240px + 305px);}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1560773505001"]{top: 587px;left: calc(50% - 160px + 250px);}}#rec103214131 .tn-elem[data-elem-id="1560773564989"]{z-index:279;top: 8px;left: calc(50% - 600px + 270px);width:325px;height:252px;}#rec103214131 .tn-elem[data-elem-id="1560773564989"] .tn-atom{background-color:#c9c9c9;background-position:center center;background-size:cover;background-repeat:no-repeat;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1560773564989"]{top: 20px;left: calc(50% - 480px + 250px);width:220px;height:165px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1560773564989"]{top: 10px;left: calc(50% - 320px + 325px);width:295px;height:222px;}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1560773564989"]{top: 10px;left: calc(50% - 240px + 245px);width:225px;height:160px;}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1560773564989"]{top: 164px;left: calc(50% - 160px + 42px);width:241px;height:145px;}}#rec103214131 .tn-elem[data-elem-id="1560773565007"]{z-index:281;top: 217px;left: calc(50% - 600px + 550px);width:30px;}#rec103214131 .tn-elem[data-elem-id="1560773565007"] .tn-atom{opacity:0.95;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1560773565007"]{top: 158px;left: calc(50% - 480px + 440px);width:20px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1560773565007"]{top: 198px;left: calc(50% - 320px + 590px);}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1560773565007"]{top: 138px;left: calc(50% - 240px + 440px);}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1560773565007"]{top: 278px;left: calc(50% - 160px + 251px);}}#rec103214131 .tn-elem[data-elem-id="1560773565019"]{color:#ffffff;text-align:center;z-index:282;top: 74px;left: calc(50% - 600px + 270px);width:320px;}#rec103214131 .tn-elem[data-elem-id="1560773565019"] .tn-atom{color:#ffffff;font-size:20px;font-family:'Open Sans';line-height:1;font-weight:600;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1560773565019"]{top: 74px;left: calc(50% - 480px + 250px);width:220px;}#rec103214131 .tn-elem[data-elem-id="1560773565019"] .tn-atom{font-size:14px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1560773565019"]{top: 95px;left: calc(50% - 320px + 330px);width:280px;}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1560773565019"]{top: 60px;left: calc(50% - 240px + 250px);width:220px;}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1560773565019"]{top: 214px;left: calc(50% - 160px + 40px);width:240px;}#rec103214131 .tn-elem[data-elem-id="1560773565019"] .tn-atom{font-size:14px;}}#rec103214131 .tn-elem[data-elem-id="1560773565024"]{color:#ffffff;text-align:center;z-index:283;top: 153px;left: calc(50% - 600px + 280px);width:300px;}#rec103214131 .tn-elem[data-elem-id="1560773565024"] .tn-atom{color:#ffffff;font-size:11px;font-family:'Open Sans';line-height:1.55;font-weight:400;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1560773565024"]{top: 120px;left: calc(50% - 480px + 250px);width:220px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1560773565024"]{top: 130px;left: calc(50% - 320px + 330px);width:290px;}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1560773565024"]{top: 100px;left: calc(50% - 240px + 250px);width:220px;}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1560773565024"]{top: 240px;left: calc(50% - 160px + 40px);width:240px;}}#rec103214131 .tn-elem[data-elem-id="1560773565028"]{z-index:280;top: 12px;left: calc(50% - 600px + 274px);width:317px;height:243px;}#rec103214131 .tn-elem[data-elem-id="1560773565028"] .tn-atom{opacity:0.3;background-color:#000000;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1560773565028"]{top: 22px;left: calc(50% - 480px + 252px);width:216px;height:161px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1560773565028"]{top: 12px;left: calc(50% - 320px + 327px);width:291px;height:218px;}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1560773565028"]{top: 12px;left: calc(50% - 240px + 247px);width:221px;height:156px;}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1560773565028"]{top: 166px;left: calc(50% - 160px + 44px);width:237px;height:141px;}}#rec103214131 .tn-elem[data-elem-id="1560773565038"]{color:#ffffff;text-align:center;z-index:284;top: 217px;left: calc(50% - 600px + 375px);width:110px;height:25px;}#rec103214131 .tn-elem[data-elem-id="1560773565038"] .tn-atom{color:#ffffff;font-size:11px;font-family:'Open Sans';line-height:1.55;font-weight:600;border-width:1px;opacity:0.85;background-color:#3d3d3d;background-position:center center;border-color:#000000;border-style:solid;transition: background-color 0.1s ease-in-out, color 0.1s ease-in-out, border-color 0.1s ease-in-out;}#rec103214131 .tn-elem[data-elem-id="1560773565038"] .tn-atom:hover{background-color:#ce4d46;color:#ffffff;}@media screen and (max-width: 1199px){#rec103214131 .tn-elem[data-elem-id="1560773565038"]{top: 160px;left: calc(50% - 480px + 315px);width:90px;height:17px;}#rec103214131 .tn-elem[data-elem-id="1560773565038"] .tn-atom{font-size:10px;}}@media screen and (max-width: 959px){#rec103214131 .tn-elem[data-elem-id="1560773565038"]{top: 200px;left: calc(50% - 320px + 425px);}}@media screen and (max-width: 639px){#rec103214131 .tn-elem[data-elem-id="1560773565038"]{top: 140px;left: calc(50% - 240px + 315px);}}@media screen and (max-width: 479px){#rec103214131 .tn-elem[data-elem-id="1560773565038"]{top: 280px;left: calc(50% - 160px + 115px);}}</style>  
  
  
  

<div class='t396'>

	<div class="t396__artboard" data-artboard-recid="103214131"
		data-artboard-height="275"
		data-artboard-height-res-960="205"		data-artboard-height-res-640="475"		data-artboard-height-res-480="350"		data-artboard-height-res-320="630"
		data-artboard-height_vh=""
										
		data-artboard-valign="center"
										
		data-artboard-ovrflw=""
	>
    
      	  <div class="t396__carrier" data-artboard-recid="103214131"></div>      
            
      <div class="t396__filter" data-artboard-recid="103214131"></div>
    
	                        
	  <div class='t396__elem tn-elem tn-elem__1032141311510752425003' data-elem-id='1510752425003' data-elem-type='text'
		data-field-top-value="90"
		data-field-top-res-960-value="87"		data-field-top-res-640-value="337"		data-field-top-res-480-value="235"		data-field-top-res-320-value="517"			
			
		data-field-left-value="958"
		data-field-left-res-960-value="738"		data-field-left-res-640-value="328"		data-field-left-res-480-value="257"		data-field-left-res-320-value="46"			

													

		data-field-width-value="300"
		data-field-width-res-960-value="210"		data-field-width-res-640-value="290"		data-field-width-res-480-value="220"		data-field-width-res-320-value="230"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
            
        
          <div class='tn-atom' field='tn_text_1510752425003'  ></div>
        
                
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311558429982717' data-elem-id='1558429982717' data-elem-type='shape'
		data-field-top-value="8"
		data-field-top-res-960-value="20"		data-field-top-res-640-value="10"		data-field-top-res-480-value="10"		data-field-top-res-320-value="11"			
			
		data-field-left-value="-68"
		data-field-left-res-960-value="10"		data-field-left-res-640-value="20"		data-field-left-res-480-value="10"		data-field-left-res-320-value="42"			

		data-field-height-value="252"		data-field-height-res-960-value="165"		data-field-height-res-640-value="222"		data-field-height-res-480-value="160"		data-field-height-res-320-value="145"			

		data-field-width-value="325"
		data-field-width-res-960-value="220"		data-field-width-res-640-value="295"		data-field-width-res-480-value="225"		data-field-width-res-320-value="241"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                
        
                  
        
          <div class='tn-atom'   style="background-image:url('https://static.tildacdn.com/tild6230-3332-4935-b037-346438616230/photo.jpg');" >
          </div>
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311558429982739' data-elem-id='1558429982739' data-elem-type='text'
		data-field-top-value="74"
		data-field-top-res-960-value="74"		data-field-top-res-640-value="95"		data-field-top-res-480-value="60"		data-field-top-res-320-value="53"			
			
		data-field-left-value="-60"
		data-field-left-res-960-value="10"		data-field-left-res-640-value="30"		data-field-left-res-480-value="10"		data-field-left-res-320-value="50"			

													

		data-field-width-value="305"
		data-field-width-res-960-value="215"		data-field-width-res-640-value="275"		data-field-width-res-480-value="215"		data-field-width-res-320-value="225"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
            
        
          <div class='tn-atom' field='tn_text_1558429982739'  >International Portfolio Management</div>
        
                
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311558429982747' data-elem-id='1558429982747' data-elem-type='text'
		data-field-top-value="153"
		data-field-top-res-960-value="120"		data-field-top-res-640-value="130"		data-field-top-res-480-value="105"		data-field-top-res-320-value="94"			
			
		data-field-left-value="-60"
		data-field-left-res-960-value="20"		data-field-left-res-640-value="20"		data-field-left-res-480-value="12"		data-field-left-res-320-value="50"			

													

		data-field-width-value="310"
		data-field-width-res-960-value="200"		data-field-width-res-640-value="290"		data-field-width-res-480-value="210"		data-field-width-res-320-value="220"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
            
        
          <div class='tn-atom' field='tn_text_1558429982747'  >Идет прямо сейчас</div>
        
                
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311558429982753' data-elem-id='1558429982753' data-elem-type='shape'
		data-field-top-value="12"
		data-field-top-res-960-value="22"		data-field-top-res-640-value="12"		data-field-top-res-480-value="12"		data-field-top-res-320-value="13"			
			
		data-field-left-value="-64"
		data-field-left-res-960-value="12"		data-field-left-res-640-value="22"		data-field-left-res-480-value="12"		data-field-left-res-320-value="44"			

		data-field-height-value="243"		data-field-height-res-960-value="161"		data-field-height-res-640-value="218"		data-field-height-res-480-value="156"		data-field-height-res-320-value="141"			

		data-field-width-value="317"
		data-field-width-res-960-value="216"		data-field-width-res-640-value="291"		data-field-width-res-480-value="221"		data-field-width-res-320-value="237"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                
        
                  
        
          <div class='tn-atom'    >
          </div>
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311558429982762' data-elem-id='1558429982762' data-elem-type='button'
		data-field-top-value="217"
		data-field-top-res-960-value="160"		data-field-top-res-640-value="200"		data-field-top-res-480-value="140"		data-field-top-res-320-value="127"			
			
		data-field-left-value="40"
		data-field-left-res-960-value="73"		data-field-left-res-640-value="120"		data-field-left-res-480-value="78"		data-field-left-res-320-value="115"			

		data-field-height-value="25"		data-field-height-res-960-value="17"									

		data-field-width-value="110"
		data-field-width-res-960-value="90"									

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                
        
             
        
        
                  
        
          <a class='tn-atom' href="#" >Подробнее</a>
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311558429982774' data-elem-id='1558429982774' data-elem-type='image'
		data-field-top-value="217"
		data-field-top-res-960-value="158"		data-field-top-res-640-value="198"		data-field-top-res-480-value="138"		data-field-top-res-320-value="128"			
			
		data-field-left-value="210"
		data-field-left-res-960-value="200"		data-field-left-res-640-value="280"		data-field-left-res-480-value="200"		data-field-left-res-320-value="250"			

													

		data-field-width-value="30"
		data-field-width-res-960-value="20"									

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                  
        
          <div class='tn-atom'   >
                        <img class='tn-atom__img' src='https://static.tildacdn.com/tild3738-3037-4862-a162-623931336134/free_7.svg'  imgfield='tn_img_1558429982774'>
                      </div>
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311560773370396' data-elem-id='1560773370396' data-elem-type='shape'
		data-field-top-value="9"
		data-field-top-res-960-value="20"		data-field-top-res-640-value="244"		data-field-top-res-480-value="180"		data-field-top-res-320-value="317"			
			
		data-field-left-value="608"
		data-field-left-res-960-value="493"		data-field-left-res-640-value="20"		data-field-left-res-480-value="10"		data-field-left-res-320-value="42"			

		data-field-height-value="252"		data-field-height-res-960-value="165"		data-field-height-res-640-value="222"		data-field-height-res-480-value="160"		data-field-height-res-320-value="144"			

		data-field-width-value="325"
		data-field-width-res-960-value="220"		data-field-width-res-640-value="295"		data-field-width-res-480-value="225"		data-field-width-res-320-value="241"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                
        
                  
        
          <div class='tn-atom'   style="background-image:url('https://static.tildacdn.com/tild6335-3331-4130-b436-666632323934/photo-1524438447314-.jpg');" >
          </div>
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311560773370434' data-elem-id='1560773370434' data-elem-type='shape'
		data-field-top-value="13"
		data-field-top-res-960-value="22"		data-field-top-res-640-value="246"		data-field-top-res-480-value="183"		data-field-top-res-320-value="318"			
			
		data-field-left-value="612"
		data-field-left-res-960-value="495"		data-field-left-res-640-value="22"		data-field-left-res-480-value="12"		data-field-left-res-320-value="44"			

		data-field-height-value="243"		data-field-height-res-960-value="161"		data-field-height-res-640-value="218"		data-field-height-res-480-value="156"		data-field-height-res-320-value="141"			

		data-field-width-value="317"
		data-field-width-res-960-value="216"		data-field-width-res-640-value="291"		data-field-width-res-480-value="221"		data-field-width-res-320-value="237"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                
        
                  
        
          <div class='tn-atom'    >
          </div>
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311560773370453' data-elem-id='1560773370453' data-elem-type='image'
		data-field-top-value="218"
		data-field-top-res-960-value="158"		data-field-top-res-640-value="433"		data-field-top-res-480-value="309"		data-field-top-res-320-value="430"			
			
		data-field-left-value="888"
		data-field-left-res-960-value="200"		data-field-left-res-640-value="286"		data-field-left-res-480-value="205"		data-field-left-res-320-value="250"			

													

		data-field-width-value="30"
		data-field-width-res-960-value="20"									

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                  
        
          <div class='tn-atom'   >
                        <img class='tn-atom__img' src='https://static.tildacdn.com/tild3738-3037-4862-a162-623931336134/free_7.svg'  imgfield='tn_img_1560773370453'>
                      </div>
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311560773370466' data-elem-id='1560773370466' data-elem-type='button'
		data-field-top-value="218"
		data-field-top-res-960-value="160"		data-field-top-res-640-value="432"		data-field-top-res-480-value="310"		data-field-top-res-320-value="429"			
			
		data-field-left-value="718"
		data-field-left-res-960-value="556"		data-field-left-res-640-value="126"		data-field-left-res-480-value="83"		data-field-left-res-320-value="115"			

		data-field-height-value="25"		data-field-height-res-960-value="17"									

		data-field-width-value="110"
		data-field-width-res-960-value="90"									

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                
        
             
        
        
                  
        
          <a class='tn-atom' href="#" >Подробнее</a>
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311560773370477' data-elem-id='1560773370477' data-elem-type='text'
		data-field-top-value="153"
		data-field-top-res-960-value="120"		data-field-top-res-640-value="372"		data-field-top-res-480-value="271"		data-field-top-res-320-value="398"			
			
		data-field-left-value="618"
		data-field-left-res-960-value="503"		data-field-left-res-640-value="26"		data-field-left-res-480-value="15"		data-field-left-res-320-value="40"			

													

		data-field-width-value="310"
		data-field-width-res-960-value="200"		data-field-width-res-640-value="290"		data-field-width-res-480-value="220"		data-field-width-res-320-value="240"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
            
        
          <div class='tn-atom' field='tn_text_1560773370477'  >Доступен весь год</div>
        
                
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311560773370483' data-elem-id='1560773370483' data-elem-type='text'
		data-field-top-value="74"
		data-field-top-res-960-value="74"		data-field-top-res-640-value="315"		data-field-top-res-480-value="225"		data-field-top-res-320-value="354"			
			
		data-field-left-value="618"
		data-field-left-res-960-value="498"		data-field-left-res-640-value="26"		data-field-left-res-480-value="15"		data-field-left-res-320-value="51"			

													

		data-field-width-value="315"
		data-field-width-res-960-value="215"		data-field-width-res-640-value="285"		data-field-width-res-480-value="215"		data-field-width-res-320-value="225"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
            
        
          <div class='tn-atom' field='tn_text_1560773370483'  >Принятие решений.<br>
Родителям
<br></div>
        
                
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311560773370489' data-elem-id='1560773370489' data-elem-type='shape'
		data-field-top-value="9"
		data-field-top-res-960-value="20"		data-field-top-res-640-value="244"		data-field-top-res-480-value="180"		data-field-top-res-320-value="472"			
			
		data-field-left-value="948"
		data-field-left-res-960-value="729"		data-field-left-res-640-value="326"		data-field-left-res-480-value="245"		data-field-left-res-320-value="42"			

		data-field-height-value="252"		data-field-height-res-960-value="165"		data-field-height-res-640-value="222"		data-field-height-res-480-value="160"		data-field-height-res-320-value="145"			

		data-field-width-value="325"
		data-field-width-res-960-value="220"		data-field-width-res-640-value="295"		data-field-width-res-480-value="225"		data-field-width-res-320-value="241"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                
        
                  
        
          <div class='tn-atom'   style="background-image:url('https://static.tildacdn.com/tild6131-3232-4837-b539-353363666461/_-2.jpg');" >
          </div>
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311560773370499' data-elem-id='1560773370499' data-elem-type='text'
		data-field-top-value="74"
		data-field-top-res-960-value="74"		data-field-top-res-640-value="314"		data-field-top-res-480-value="224"		data-field-top-res-320-value="512"			
			
		data-field-left-value="1000"
		data-field-left-res-960-value="737"		data-field-left-res-640-value="336"		data-field-left-res-480-value="251"		data-field-left-res-320-value="52"			

													

		data-field-width-value="225"
		data-field-width-res-960-value="215"		data-field-width-res-640-value="275"		data-field-width-res-480-value="215"		data-field-width-res-320-value="225"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
            
        
          <div class='tn-atom' field='tn_text_1560773370499'  >Блокчейн <br>
и криптовалюты</div>
        
                
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311560773370505' data-elem-id='1560773370505' data-elem-type='text'
		data-field-top-value="153"
		data-field-top-res-960-value="120"		data-field-top-res-640-value="364"		data-field-top-res-480-value="270"		data-field-top-res-320-value="557"			
			
		data-field-left-value="958"
		data-field-left-res-960-value="747"		data-field-left-res-640-value="328"		data-field-left-res-480-value="247"		data-field-left-res-320-value="50"			

													

		data-field-width-value="310"
		data-field-width-res-960-value="200"		data-field-width-res-640-value="290"		data-field-width-res-480-value="220"		data-field-width-res-320-value="230"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
            
        
          <div class='tn-atom' field='tn_text_1560773370505'  >Идет прямо сейчас</div>
        
                
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311560773370510' data-elem-id='1560773370510' data-elem-type='shape'
		data-field-top-value="13"
		data-field-top-res-960-value="22"		data-field-top-res-640-value="246"		data-field-top-res-480-value="182"		data-field-top-res-320-value="474"			
			
		data-field-left-value="952"
		data-field-left-res-960-value="731"		data-field-left-res-640-value="328"		data-field-left-res-480-value="247"		data-field-left-res-320-value="44"			

		data-field-height-value="243"		data-field-height-res-960-value="161"		data-field-height-res-640-value="218"		data-field-height-res-480-value="156"		data-field-height-res-320-value="141"			

		data-field-width-value="317"
		data-field-width-res-960-value="216"		data-field-width-res-640-value="291"		data-field-width-res-480-value="221"		data-field-width-res-320-value="237"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                
        
                  
        
          <div class='tn-atom'    >
          </div>
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311560773370520' data-elem-id='1560773370520' data-elem-type='button'
		data-field-top-value="218"
		data-field-top-res-960-value="158"		data-field-top-res-640-value="433"		data-field-top-res-480-value="310"		data-field-top-res-320-value="588"			
			
		data-field-left-value="1056"
		data-field-left-res-960-value="793"		data-field-left-res-640-value="426"		data-field-left-res-480-value="313"		data-field-left-res-320-value="115"			

		data-field-height-value="25"		data-field-height-res-960-value="17"									

		data-field-width-value="110"
		data-field-width-res-960-value="90"									

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                
        
             
        
        
                  
        
          <a class='tn-atom' href="#" >Подробнее</a>
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311560773458443' data-elem-id='1560773458443' data-elem-type='image'
		data-field-top-value="218"
		data-field-top-res-960-value="158"		data-field-top-res-640-value="523"		data-field-top-res-480-value="309"		data-field-top-res-320-value="660"			
			
		data-field-left-value="1229"
		data-field-left-res-960-value="680"		data-field-left-res-640-value="306"		data-field-left-res-480-value="442"		data-field-left-res-320-value="300"			

													

		data-field-width-value="30"
		data-field-width-res-960-value="20"									

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                  
        
          <div class='tn-atom'   >
                        <img class='tn-atom__img' src='https://static.tildacdn.com/tild3738-3037-4862-a162-623931336134/free_7.svg'  imgfield='tn_img_1560773458443'>
                      </div>
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311560773505001' data-elem-id='1560773505001' data-elem-type='image'
		data-field-top-value="228px"
		data-field-top-res-960-value="158"		data-field-top-res-640-value="434"		data-field-top-res-480-value="419"		data-field-top-res-320-value="587"			
			
		data-field-left-value="1239px"
		data-field-left-res-960-value="920"		data-field-left-res-640-value="591"		data-field-left-res-480-value="305"		data-field-left-res-320-value="250"			

													

		data-field-width-value="30"
		data-field-width-res-960-value="20"									

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                  
        
          <div class='tn-atom'   >
                        <img class='tn-atom__img' src='https://static.tildacdn.com/tild3738-3037-4862-a162-623931336134/free_7.svg'  imgfield='tn_img_1560773505001'>
                      </div>
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311560773564989' data-elem-id='1560773564989' data-elem-type='shape'
		data-field-top-value="8"
		data-field-top-res-960-value="20"		data-field-top-res-640-value="10"		data-field-top-res-480-value="10"		data-field-top-res-320-value="164"			
			
		data-field-left-value="270"
		data-field-left-res-960-value="250"		data-field-left-res-640-value="325"		data-field-left-res-480-value="245"		data-field-left-res-320-value="42"			

		data-field-height-value="252"		data-field-height-res-960-value="165"		data-field-height-res-640-value="222"		data-field-height-res-480-value="160"		data-field-height-res-320-value="145"			

		data-field-width-value="325"
		data-field-width-res-960-value="220"		data-field-width-res-640-value="295"		data-field-width-res-480-value="225"		data-field-width-res-320-value="241"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                
        
                  
        
          <div class='tn-atom'   style="background-image:url('https://static.tildacdn.com/tild3039-3037-4739-b164-386536323966/__.jpg');" >
          </div>
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311560773565007' data-elem-id='1560773565007' data-elem-type='image'
		data-field-top-value="217"
		data-field-top-res-960-value="158"		data-field-top-res-640-value="198"		data-field-top-res-480-value="138"		data-field-top-res-320-value="278"			
			
		data-field-left-value="550"
		data-field-left-res-960-value="440"		data-field-left-res-640-value="590"		data-field-left-res-480-value="440"		data-field-left-res-320-value="251"			

													

		data-field-width-value="30"
		data-field-width-res-960-value="20"									

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                  
        
          <div class='tn-atom'   >
                        <img class='tn-atom__img' src='https://static.tildacdn.com/tild3738-3037-4862-a162-623931336134/free_7.svg'  imgfield='tn_img_1560773565007'>
                      </div>
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311560773565019' data-elem-id='1560773565019' data-elem-type='text'
		data-field-top-value="74"
		data-field-top-res-960-value="74"		data-field-top-res-640-value="95"		data-field-top-res-480-value="60"		data-field-top-res-320-value="214"			
			
		data-field-left-value="270"
		data-field-left-res-960-value="250"		data-field-left-res-640-value="330"		data-field-left-res-480-value="250"		data-field-left-res-320-value="40"			

													

		data-field-width-value="320"
		data-field-width-res-960-value="220"		data-field-width-res-640-value="280"		data-field-width-res-480-value="220"		data-field-width-res-320-value="240"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
            
        
          <div class='tn-atom' field='tn_text_1560773565019'  >Физика для инопланетян</div>
        
                
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311560773565024' data-elem-id='1560773565024' data-elem-type='text'
		data-field-top-value="153"
		data-field-top-res-960-value="120"		data-field-top-res-640-value="130"		data-field-top-res-480-value="100"		data-field-top-res-320-value="240"			
			
		data-field-left-value="280"
		data-field-left-res-960-value="250"		data-field-left-res-640-value="330"		data-field-left-res-480-value="250"		data-field-left-res-320-value="40"			

													

		data-field-width-value="300"
		data-field-width-res-960-value="220"		data-field-width-res-640-value="290"		data-field-width-res-480-value="220"		data-field-width-res-320-value="240"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
            
        
          <div class='tn-atom' field='tn_text_1560773565024'  >Идет прямо сейчас</div>
        
                
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311560773565028' data-elem-id='1560773565028' data-elem-type='shape'
		data-field-top-value="12"
		data-field-top-res-960-value="22"		data-field-top-res-640-value="12"		data-field-top-res-480-value="12"		data-field-top-res-320-value="166"			
			
		data-field-left-value="274"
		data-field-left-res-960-value="252"		data-field-left-res-640-value="327"		data-field-left-res-480-value="247"		data-field-left-res-320-value="44"			

		data-field-height-value="243"		data-field-height-res-960-value="161"		data-field-height-res-640-value="218"		data-field-height-res-480-value="156"		data-field-height-res-320-value="141"			

		data-field-width-value="317"
		data-field-width-res-960-value="216"		data-field-width-res-640-value="291"		data-field-width-res-480-value="221"		data-field-width-res-320-value="237"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                
        
                  
        
          <div class='tn-atom'    >
          </div>
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__1032141311560773565038' data-elem-id='1560773565038' data-elem-type='button'
		data-field-top-value="217"
		data-field-top-res-960-value="160"		data-field-top-res-640-value="200"		data-field-top-res-480-value="140"		data-field-top-res-320-value="280"			
			
		data-field-left-value="375"
		data-field-left-res-960-value="315"		data-field-left-res-640-value="425"		data-field-left-res-480-value="315"		data-field-left-res-320-value="115"			

		data-field-height-value="25"		data-field-height-res-960-value="17"									

		data-field-width-value="110"
		data-field-width-res-960-value="90"									

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                
        
             
        
        
                  
        
          <a class='tn-atom' href="#" >Подробнее</a>
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                                                                                                                                                                                                                                      
    </div>
  
</div>  


<script>
$( document ).ready(function() {  
  t396_init('103214131');  
});
</script>



<!-- /T396 -->
</div>


<div id="rec36935564" class="r t-rec t-rec_pt_0 t-rec_pb_0" style="padding-top:0px;padding-bottom:0px; "  data-record-type="191"   >
<!-- T142 -->
<div class="t142">
	<div class="t-container_100">
	  <div class="t142__wrapone">
		<div class="t142__wraptwo">
          <a href="#" target=""  class=""><div class="t-btn t142__submit t142__submit_size_sm" style="color:#000000;border:1px solid #000000;" >Еще</div></a>
        </div>
	  </div>
	</div>
</div>


<script type="text/javascript">
  $(document).ready(function() {
    t142_checkSize('36935564');
  });
    $(window).load(function() {
      t142_checkSize('36935564');
    });
</script>




		
		
		
	
	<style>
	#rec36935564 .t-btn:not(.t-animate_no-hover):hover{
	  background-color: #ce4d46 !important;
	  color: #ffffff !important;
	  
	  
	}	
	#rec36935564 .t-btn:not(.t-animate_no-hover){
	  -webkit-transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out, border-color 0.2s ease-in-out, box-shadow 0.2s ease-in-out; transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out, border-color 0.2s ease-in-out, box-shadow 0.2s ease-in-out;
	}
	</style>
	
	

<style>

#rec36935564 .t-btn[data-btneffects-first],
#rec36935564 .t-btn[data-btneffects-second],
#rec36935564 .t-submit[data-btneffects-first],
#rec36935564 .t-submit[data-btneffects-second] {
	position: relative;
  overflow: hidden;
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
}





</style>



<script type="text/javascript">

$(document).ready(function() {
	

	

});

</script>
</div>


<div id="rec34760814" class="r t-rec t-rec_pt_135 t-rec_pb_0" style="padding-top:135px;padding-bottom:0px; "  data-record-type="33"   >
<!-- T017 -->
<div class="t017">
  <div class="t-container t-align_center">
    <div class="t-col t-col_12 ">      
            <div class="t017__title t-title t-title_xxs" field="title" style=""><div style="font-size:38px;font-family:'Intro';color:#000000;" data-customstyle="yes"><span style="font-weight: 600;">Последние лекции</span></div></div>          </div>
  </div>
</div>
</div>


<div id="rec34763468" class="r t-rec t-rec_pt_15 t-rec_pb_0" style="padding-top:15px;padding-bottom:0px; " data-animationappear="off" data-record-type="604"   >





<div class="t604">
  <div class="t-slds" style="visibility: hidden;">
    <div class="t-container t-slds__main">
      <div class="t-slds__container t-width t-width_ t-margin_auto">
        
        <div class="t-slds__items-wrapper  t-slds_animated-fast t-slds__witharrows" data-slider-transition="300" data-slider-with-cycle="true" data-slider-correct-height="false" data-auto-correct-mobile-width="false" data-slider-arrows-nearpic="yes"  >
                      <div class="t-slds__item  t-slds__item_active" data-slide-index="1">
              <div class="t-width t-width_12 t-margin_auto" itemscope itemtype="http://schema.org/ImageObject">
                <div class="t-slds__wrapper t-align_center" >
                                      <meta itemprop="image" content="https://static.tildacdn.com/tild3437-6131-4566-a132-353434333766/maxresdefault.jpg">                    <div class="t604__imgwrapper" bgimgfield="gi_img__0" >
                      <div class="t-slds__bgimg  t-bgimg" data-original="https://static.tildacdn.com/tild3437-6131-4566-a132-353434333766/maxresdefault.jpg" style="background-image: url('https://static.tildacdn.com/tild3437-6131-4566-a132-353434333766/maxresdefault.jpg');"></div>
                      <div class="t604__separator" data-slider-image-width="1160" data-slider-image-height="550"></div>                      <div class="t604__play" data-slider-video-type="youtube" data-slider-video-url="U9qOiZYa-n4">
                        <div class="t604__play_icon" style="width: 70px; height: 70px;">
                          <svg width="70px" height="70px" viewBox="0 0 60 60">
                            <g stroke="none" stroke-width="1" fill="" fill-rule="evenodd">
                            <g transform="translate(-691.000000, -3514.000000)" fill="#ffffff">
                              <path d="M721,3574 C737.568542,3574 751,3560.56854 751,3544 C751,3527.43146 737.568542,3514 721,3514 C704.431458,3514 691,3527.43146 691,3544 C691,3560.56854 704.431458,3574 721,3574 Z M715,3534 L732,3544.5 L715,3555 L715,3534 Z"></path>
                            </g>
                            </g>
                          </svg>
                        </div>
                      </div>
                      <div class="t604__frame"></div>
                    </div>
                                  </div>
              </div>
            </div>
          
                      <div class="t-slds__item  " data-slide-index="2">
              <div class="t-width t-width_12 t-margin_auto" itemscope itemtype="http://schema.org/ImageObject">
                <div class="t-slds__wrapper t-align_center" >
                                      <meta itemprop="image" content="https://static.tildacdn.com/tild3637-6635-4833-a434-376534333038/maxresdefault.jpg">                    <div class="t604__imgwrapper" bgimgfield="gi_img__1" >
                      <div class="t-slds__bgimg  t-bgimg" data-original="https://static.tildacdn.com/tild3637-6635-4833-a434-376534333038/maxresdefault.jpg" style="background-image: url('https://static.tildacdn.com/tild3637-6635-4833-a434-376534333038/maxresdefault.jpg');"></div>
                      <div class="t604__separator" data-slider-image-width="1160" data-slider-image-height="550"></div>                      <div class="t604__play" data-slider-video-type="youtube" data-slider-video-url="9ZAMC0V07TA">
                        <div class="t604__play_icon" style="width: 70px; height: 70px;">
                          <svg width="70px" height="70px" viewBox="0 0 60 60">
                            <g stroke="none" stroke-width="1" fill="" fill-rule="evenodd">
                            <g transform="translate(-691.000000, -3514.000000)" fill="#ffffff">
                              <path d="M721,3574 C737.568542,3574 751,3560.56854 751,3544 C751,3527.43146 737.568542,3514 721,3514 C704.431458,3514 691,3527.43146 691,3544 C691,3560.56854 704.431458,3574 721,3574 Z M715,3534 L732,3544.5 L715,3555 L715,3534 Z"></path>
                            </g>
                            </g>
                          </svg>
                        </div>
                      </div>
                      <div class="t604__frame"></div>
                    </div>
                                  </div>
              </div>
            </div>
          
                      <div class="t-slds__item  " data-slide-index="3">
              <div class="t-width t-width_12 t-margin_auto" itemscope itemtype="http://schema.org/ImageObject">
                <div class="t-slds__wrapper t-align_center" >
                                      <meta itemprop="image" content="https://static.tildacdn.com/tild3231-6234-4564-a465-363764326339/maxresdefault.jpg">                    <div class="t604__imgwrapper" bgimgfield="gi_img__2" >
                      <div class="t-slds__bgimg  t-bgimg" data-original="https://static.tildacdn.com/tild3231-6234-4564-a465-363764326339/maxresdefault.jpg" style="background-image: url('https://static.tildacdn.com/tild3231-6234-4564-a465-363764326339/maxresdefault.jpg');"></div>
                      <div class="t604__separator" data-slider-image-width="1160" data-slider-image-height="550"></div>                      <div class="t604__play" data-slider-video-type="youtube" data-slider-video-url="o-2_EKjrnRI">
                        <div class="t604__play_icon" style="width: 70px; height: 70px;">
                          <svg width="70px" height="70px" viewBox="0 0 60 60">
                            <g stroke="none" stroke-width="1" fill="" fill-rule="evenodd">
                            <g transform="translate(-691.000000, -3514.000000)" fill="#ffffff">
                              <path d="M721,3574 C737.568542,3574 751,3560.56854 751,3544 C751,3527.43146 737.568542,3514 721,3514 C704.431458,3514 691,3527.43146 691,3544 C691,3560.56854 704.431458,3574 721,3574 Z M715,3534 L732,3544.5 L715,3555 L715,3534 Z"></path>
                            </g>
                            </g>
                          </svg>
                        </div>
                      </div>
                      <div class="t604__frame"></div>
                    </div>
                                  </div>
              </div>
            </div>
          
                  </div>
              </div>
      
        <div class="t-slds__arrow_container   t-slds__arrow-nearpic">
                  


<div class="t-slds__arrow_wrapper t-slds__arrow_wrapper-left" data-slide-direction="left">
  <div class="t-slds__arrow t-slds__arrow-left t-slds__arrow-withbg" style="width: 50px; height: 50px;background-color: rgba(0,0,0,1);">
    <div class="t-slds__arrow_body t-slds__arrow_body-left" style="width: 12px;">
      <svg style="display: block" viewBox="0 0 12.6 22" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <desc>Left</desc>
        <polyline
        fill="none" 
        stroke="#ffffff"
        stroke-linejoin="butt" 
        stroke-linecap="butt"
        stroke-width="2" 
        points="1,1 11,11 1,21" 
        />
      </svg>
    </div>
  </div>
</div>
<div class="t-slds__arrow_wrapper t-slds__arrow_wrapper-right" data-slide-direction="right">
  <div class="t-slds__arrow t-slds__arrow-right t-slds__arrow-withbg" style="width: 50px; height: 50px;background-color: rgba(0,0,0,1);">
    <div class="t-slds__arrow_body t-slds__arrow_body-right" style="width: 12px;">
      <svg style="display: block" viewBox="0 0 12.6 22" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <desc>Right</desc>
        <polyline
        fill="none" 
        stroke="#ffffff"
        stroke-linejoin="butt" 
        stroke-linecap="butt"
        stroke-width="2" 
        points="1,1 11,11 1,21" 
        />
      </svg>
    </div>
  </div>
</div>        </div>
            <div class="t-slds__caption__container">
                                                      </div>
    </div>
    
  </div>
</div>


<script type="text/javascript">
  $(document).ready(function() {
    t_sldsInit('34763468');
    t604_init('34763468');
    
    var fireRefreshEventOnWindow = function () {
         var evt = document.createEvent("HTMLEvents");
         evt.initEvent('resize', true, false);
         window.dispatchEvent(evt);
     };
    $('.t604').bind('displayChanged',function(){
      fireRefreshEventOnWindow();
    });
  }); 
</script>



<style type="text/css">
  #rec34763468 .t-slds__bullet_active .t-slds__bullet_body {
    background-color: #222 !important;
  }
  
  #rec34763468 .t-slds__bullet:hover .t-slds__bullet_body {
    background-color: #222 !important;
  }
</style>

</div>


<div id="rec36935690" class="r t-rec t-rec_pt_0 t-rec_pb_135" style="padding-top:0px;padding-bottom:135px; "  data-record-type="191"   >
<!-- T142 -->
<div class="t142">
	<div class="t-container_100">
	  <div class="t142__wrapone">
		<div class="t142__wraptwo">
          <a href="#" target=""  class=""><div class="t-btn t142__submit t142__submit_size_sm" style="color:#000000;border:1px solid #000000;" >Еще</div></a>
        </div>
	  </div>
	</div>
</div>


<script type="text/javascript">
  $(document).ready(function() {
    t142_checkSize('36935690');
  });
    $(window).load(function() {
      t142_checkSize('36935690');
    });
</script>




		
		
		
	
	<style>
	#rec36935690 .t-btn:not(.t-animate_no-hover):hover{
	  background-color: #ce4d46 !important;
	  color: #ffffff !important;
	  
	  
	}	
	#rec36935690 .t-btn:not(.t-animate_no-hover){
	  -webkit-transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out, border-color 0.2s ease-in-out, box-shadow 0.2s ease-in-out; transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out, border-color 0.2s ease-in-out, box-shadow 0.2s ease-in-out;
	}
	</style>
	
	

<style>

#rec36935690 .t-btn[data-btneffects-first],
#rec36935690 .t-btn[data-btneffects-second],
#rec36935690 .t-submit[data-btneffects-first],
#rec36935690 .t-submit[data-btneffects-second] {
	position: relative;
  overflow: hidden;
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
}





</style>



<script type="text/javascript">

$(document).ready(function() {
	

	

});

</script>
</div>


<div id="rec52079325" class="r t-rec" style=" " data-animationappear="off" data-record-type="396"   >
<!-- T396 -->
<style>#rec52079325 .t396__artboard{height: 520px;}#rec52079325 .t396__filter{height: 520px;}#rec52079325 .t396__carrier{height: 520px;background-position: center center;background-attachment: scroll;background-size:cover;background-repeat:no-repeat;}@media screen and (max-width: 1199px){#rec52079325 .t396__artboard{height: 450px;}#rec52079325 .t396__filter{height: 450px;}#rec52079325 .t396__carrier{height: 450px;background-attachment:scroll;}}@media screen and (max-width: 959px){#rec52079325 .t396__artboard{height: 350px;}#rec52079325 .t396__filter{height: 350px;}#rec52079325 .t396__carrier{height: 350px;}}@media screen and (max-width: 639px){#rec52079325 .t396__artboard{height: 250px;}#rec52079325 .t396__filter{height: 250px;}#rec52079325 .t396__carrier{height: 250px;}}@media screen and (max-width: 479px){#rec52079325 .t396__artboard{height: 200px;}#rec52079325 .t396__filter{height: 200px;}#rec52079325 .t396__carrier{height: 200px;}}#rec52079325 .tn-elem[data-elem-id="1470209944682"]{color:#000000;text-align:center;z-index:7;top: 30px;left: calc(50% - 600px + 0px);width:1182px;}#rec52079325 .tn-elem[data-elem-id="1470209944682"] .tn-atom{color:#000000;font-size:38px;font-family:'Intro';line-height:0.95;font-weight:700;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec52079325 .tn-elem[data-elem-id="1470209944682"]{top: 40px;left: calc(50% - 480px + -87px);}#rec52079325 .tn-elem[data-elem-id="1470209944682"] .tn-atom{font-size:32px;}}@media screen and (max-width: 959px){#rec52079325 .tn-elem[data-elem-id="1470209944682"]{top: 30px;left: calc(50% - 320px + 3px);width:632px;}#rec52079325 .tn-elem[data-elem-id="1470209944682"] .tn-atom{font-size:23px;}}@media screen and (max-width: 639px){#rec52079325 .tn-elem[data-elem-id="1470209944682"]{top: 20px;left: calc(50% - 240px + 0px);width:482px;}#rec52079325 .tn-elem[data-elem-id="1470209944682"] .tn-atom{font-size:22px;}}@media screen and (max-width: 479px){#rec52079325 .tn-elem[data-elem-id="1470209944682"]{top: 20px;left: calc(50% - 160px + 5px);width:312px;}#rec52079325 .tn-elem[data-elem-id="1470209944682"] .tn-atom{font-size:15px;}}#rec52079325 .tn-elem[data-elem-id="1523974687217"]{color:#000000;text-align:center;z-index:22;top: 80px;left: calc(50% - 600px + -30px);width:1180px;}#rec52079325 .tn-elem[data-elem-id="1523974687217"] .tn-atom{color:#000000;font-size:18px;font-family:'Open Sans';line-height:1.55;font-weight:500;background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec52079325 .tn-elem[data-elem-id="1523974687217"]{top: 80px;left: calc(50% - 480px + 10px);width:960px;}}@media screen and (max-width: 959px){#rec52079325 .tn-elem[data-elem-id="1523974687217"]{top: 60px;left: calc(50% - 320px + 10px);width:620px;}#rec52079325 .tn-elem[data-elem-id="1523974687217"] .tn-atom{font-size:16px;}}@media screen and (max-width: 639px){#rec52079325 .tn-elem[data-elem-id="1523974687217"]{top: 50px;left: calc(50% - 240px + 10px);width:460px;}#rec52079325 .tn-elem[data-elem-id="1523974687217"] .tn-atom{font-size:14px;}}@media screen and (max-width: 479px){#rec52079325 .tn-elem[data-elem-id="1523974687217"]{top: 40px;left: calc(50% - 160px + 10px);width:300px;}#rec52079325 .tn-elem[data-elem-id="1523974687217"] .tn-atom{font-size:10px;}}#rec52079325 .tn-elem[data-elem-id="1524131797920"]{color:#000000;text-align:center;z-index:24;top: 440px;left: calc(50% - 600px + 490px);width:120px;height:40px;}#rec52079325 .tn-elem[data-elem-id="1524131797920"] .tn-atom{color:#000000;font-size:14px;font-family:'Intro';line-height:1.55;font-weight:600;border-width:2px;border-radius:0px;background-position:center center;border-color:#000000;border-style:solid;transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out, border-color 0.2s ease-in-out;}#rec52079325 .tn-elem[data-elem-id="1524131797920"] .tn-atom:hover{background-color:#ce4d46;color:#ffffff;}@media screen and (max-width: 1199px){#rec52079325 .tn-elem[data-elem-id="1524131797920"]{top: 378px;left: calc(50% - 480px + 400px);width:90px;height:30px;}#rec52079325 .tn-elem[data-elem-id="1524131797920"] .tn-atom{font-size:12px;}}@media screen and (max-width: 959px){#rec52079325 .tn-elem[data-elem-id="1524131797920"]{top: 280px;left: calc(50% - 320px + 270px);width:80px;height:30px;}#rec52079325 .tn-elem[data-elem-id="1524131797920"] .tn-atom{font-size:11px;}}@media screen and (max-width: 639px){#rec52079325 .tn-elem[data-elem-id="1524131797920"]{top: 210px;left: calc(50% - 240px + 210px);width:60px;height:20px;}#rec52079325 .tn-elem[data-elem-id="1524131797920"] .tn-atom{font-size:10px;}}@media screen and (max-width: 479px){#rec52079325 .tn-elem[data-elem-id="1524131797920"]{top: 155px;left: calc(50% - 160px + 140px);width:60px;height:20px;}}#rec52079325 .tn-elem[data-elem-id="1524658796038"]{z-index:27;top: 340px;left: calc(50% - 600px + 260px);width:210px;}#rec52079325 .tn-elem[data-elem-id="1524658796038"] .tn-atom{background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec52079325 .tn-elem[data-elem-id="1524658796038"]{top: 310px;left: calc(50% - 480px + 230px);width:150px;}}@media screen and (max-width: 959px){#rec52079325 .tn-elem[data-elem-id="1524658796038"]{top: 220px;left: calc(50% - 320px + 130px);width:130px;}}@media screen and (max-width: 639px){#rec52079325 .tn-elem[data-elem-id="1524658796038"]{top: 170px;left: calc(50% - 240px + 110px);width:90px;}}@media screen and (max-width: 479px){#rec52079325 .tn-elem[data-elem-id="1524658796038"]{top: 130px;left: calc(50% - 160px + 70px);width:60px;}}#rec52079325 .tn-elem[data-elem-id="1524658810837"]{z-index:26;top: 157px;left: calc(50% - 600px + 200px);width:300px;}#rec52079325 .tn-elem[data-elem-id="1524658810837"] .tn-atom{background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec52079325 .tn-elem[data-elem-id="1524658810837"]{top: 148px;left: calc(50% - 480px + 160px);width:250px;}}@media screen and (max-width: 959px){#rec52079325 .tn-elem[data-elem-id="1524658810837"]{top: 108px;left: calc(50% - 320px + 100px);width:180px;}}@media screen and (max-width: 639px){#rec52079325 .tn-elem[data-elem-id="1524658810837"]{top: 88px;left: calc(50% - 240px + 85px);width:130px;}}@media screen and (max-width: 479px){#rec52079325 .tn-elem[data-elem-id="1524658810837"]{top: 68px;left: calc(50% - 160px + 55px);width:90px;}}#rec52079325 .tn-elem[data-elem-id="1524658824309"]{z-index:25;top: 140px;left: calc(50% - 600px + 490px);width:470px;}#rec52079325 .tn-elem[data-elem-id="1524658824309"] .tn-atom{background-position:center center;border-color:transparent;border-style:solid;}@media screen and (max-width: 1199px){#rec52079325 .tn-elem[data-elem-id="1524658824309"]{top: 130px;left: calc(50% - 480px + 400px);width:390px;}}@media screen and (max-width: 959px){#rec52079325 .tn-elem[data-elem-id="1524658824309"]{top: 100px;left: calc(50% - 320px + 270px);width:280px;}}@media screen and (max-width: 639px){#rec52079325 .tn-elem[data-elem-id="1524658824309"]{top: 80px;left: calc(50% - 240px + 210px);width:200px;}}@media screen and (max-width: 479px){#rec52079325 .tn-elem[data-elem-id="1524658824309"]{top: 60px;left: calc(50% - 160px + 140px);width:140px;}}</style>  
  
  
  

<div class='t396'>

	<div class="t396__artboard" data-artboard-recid="52079325"
		data-artboard-height="520"
		data-artboard-height-res-960="450"		data-artboard-height-res-640="350"		data-artboard-height-res-480="250"		data-artboard-height-res-320="200"
		data-artboard-height_vh=""
										
		data-artboard-valign="center"
										
		data-artboard-ovrflw=""
	>
    
      	  <div class="t396__carrier" data-artboard-recid="52079325"></div>      
            
      <div class="t396__filter" data-artboard-recid="52079325"></div>
    
	                        
	  <div class='t396__elem tn-elem tn-elem__520793251470209944682' data-elem-id='1470209944682' data-elem-type='text'
		data-field-top-value="30"
		data-field-top-res-960-value="40"		data-field-top-res-640-value="30"		data-field-top-res-480-value="20"		data-field-top-res-320-value="20"			
			
		data-field-left-value="0"
		data-field-left-res-960-value="-87"		data-field-left-res-640-value="3"		data-field-left-res-480-value="0"		data-field-left-res-320-value="5"			

													

		data-field-width-value="1182"
				data-field-width-res-640-value="632"		data-field-width-res-480-value="482"		data-field-width-res-320-value="312"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
            
        
          <div class='tn-atom' field='tn_text_1470209944682'  >современное курсостроение</div>
        
                
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__520793251523974687217' data-elem-id='1523974687217' data-elem-type='text'
		data-field-top-value="80"
		data-field-top-res-960-value="80"		data-field-top-res-640-value="60"		data-field-top-res-480-value="50"		data-field-top-res-320-value="40"			
			
		data-field-left-value="-30"
		data-field-left-res-960-value="10"		data-field-left-res-640-value="10"		data-field-left-res-480-value="10"		data-field-left-res-320-value="10"			

													

		data-field-width-value="1180"
		data-field-width-res-960-value="960"		data-field-width-res-640-value="620"		data-field-width-res-480-value="460"		data-field-width-res-320-value="300"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
            
        
          <div class='tn-atom' field='tn_text_1523974687217'  >Всё о том, как создать и запустить онлайн курс</div>
        
                
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__520793251524131797920' data-elem-id='1524131797920' data-elem-type='button'
		data-field-top-value="440"
		data-field-top-res-960-value="378"		data-field-top-res-640-value="280"		data-field-top-res-480-value="210"		data-field-top-res-320-value="155"			
			
		data-field-left-value="490"
		data-field-left-res-960-value="400"		data-field-left-res-640-value="270"		data-field-left-res-480-value="210"		data-field-left-res-320-value="140"			

		data-field-height-value="40"		data-field-height-res-960-value="30"		data-field-height-res-640-value="30"		data-field-height-res-480-value="20"		data-field-height-res-320-value="20"			

		data-field-width-value="120"
		data-field-width-res-960-value="90"		data-field-width-res-640-value="80"		data-field-width-res-480-value="60"		data-field-width-res-320-value="60"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                
        
             
        
        
                  
        
          <a class='tn-atom' href="#" >К курсу</a>
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__520793251524658796038' data-elem-id='1524658796038' data-elem-type='image'
		data-field-top-value="340"
		data-field-top-res-960-value="310"		data-field-top-res-640-value="220"		data-field-top-res-480-value="170"		data-field-top-res-320-value="130"			
			
		data-field-left-value="260"
		data-field-left-res-960-value="230"		data-field-left-res-640-value="130"		data-field-left-res-480-value="110"		data-field-left-res-320-value="70"			

													

		data-field-width-value="210"
		data-field-width-res-960-value="150"		data-field-width-res-640-value="130"		data-field-width-res-480-value="90"		data-field-width-res-320-value="60"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                  
        
          <div class='tn-atom'   >
                        <img class='tn-atom__img' src='https://static.tildacdn.com/tild3934-3361-4439-b232-353564633766/3.png'  imgfield='tn_img_1524658796038'>
                      </div>
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__520793251524658810837' data-elem-id='1524658810837' data-elem-type='image'
		data-field-top-value="157"
		data-field-top-res-960-value="148"		data-field-top-res-640-value="108"		data-field-top-res-480-value="88"		data-field-top-res-320-value="68"			
			
		data-field-left-value="200"
		data-field-left-res-960-value="160"		data-field-left-res-640-value="100"		data-field-left-res-480-value="85"		data-field-left-res-320-value="55"			

													

		data-field-width-value="300"
		data-field-width-res-960-value="250"		data-field-width-res-640-value="180"		data-field-width-res-480-value="130"		data-field-width-res-320-value="90"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                  
        
          <div class='tn-atom'   >
                        <img class='tn-atom__img' src='https://static.tildacdn.com/tild3061-3334-4336-a133-316533623736/1.png'  imgfield='tn_img_1524658810837'>
                      </div>
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                  
	  <div class='t396__elem tn-elem tn-elem__520793251524658824309' data-elem-id='1524658824309' data-elem-type='image'
		data-field-top-value="140"
		data-field-top-res-960-value="130"		data-field-top-res-640-value="100"		data-field-top-res-480-value="80"		data-field-top-res-320-value="60"			
			
		data-field-left-value="490"
		data-field-left-res-960-value="400"		data-field-left-res-640-value="270"		data-field-left-res-480-value="210"		data-field-left-res-320-value="140"			

													

		data-field-width-value="470"
		data-field-width-res-960-value="390"		data-field-width-res-640-value="280"		data-field-width-res-480-value="200"		data-field-width-res-320-value="140"			

		data-field-axisy-value="top"
											
			
		data-field-axisx-value="left"
											

		data-field-container-value="grid"
											

		data-field-topunits-value=""
											

		data-field-leftunits-value=""
									
			
		data-field-heightunits-value=""
														

		data-field-widthunits-value=""
																			
		
		     	
		      		    		
					
			
														
			
             
			
	  >

                
        
                
        
                  
        
          <div class='tn-atom'   >
                        <img class='tn-atom__img' src='https://static.tildacdn.com/tild3665-3761-4462-b838-376630316434/2.png'  imgfield='tn_img_1524658824309'>
                      </div>
        
                
        
             
        
        
                     
        
        
           
        
                  
        
             


             
        
        
      </div>
      
                                                                                                                              
    </div>
  
</div>  


<script>
$( document ).ready(function() {  
  t396_init('52079325');  
});
</script>



<!-- /T396 -->
</div>


<div id="rec52079326" class="r t-rec" style=" "  data-record-type="241"   >
<!-- T213 -->
<div class="t213" id="t213-marker52079326" data-bg-color="#ffe1b0">
  </div>

<script type="text/javascript">
 

  $(document).ready(function(){
      t213_init('52079326');
  });  

     
</script>
</div>


<div id="rec34714156" class="r t-rec t-rec_pt_0 t-rec_pb_0" style="padding-top:0px;padding-bottom:0px; " data-animationappear="off" data-record-type="607"   >
<!-- T607 -->



<div class="t607 t607__middle-pos" style=" margin-right:20px;" >
		<ul class="t607__list" >
				<li class="t607__list_item" style="padding:12.5px 0;">
			<a class="t-menu__link-item" href="#rec37330387">
				<div class="t607__tooltip t-descr t-descr_xs" style="background:#383838; line-height:2.5; border-radius:3px; color:#ffffff;font-size:14px;font-weight:600; right:40px;">Меню</div>
				<div class="t607__dot" style=" border: 2px solid #dbdbdb; width:20px; height:20px; box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.1); "></div>
			</a>
		</li>
				<li class="t607__list_item" style="padding:12.5px 0;">
			<a class="t-menu__link-item" href="#rec38915347">
				<div class="t607__tooltip t-descr t-descr_xs" style="background:#383838; line-height:2.5; border-radius:3px; color:#ffffff;font-size:14px;font-weight:600; right:40px;">Курсы </div>
				<div class="t607__dot" style=" border: 2px solid #dbdbdb; width:20px; height:20px; box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.1); "></div>
			</a>
		</li>
				<li class="t607__list_item" style="padding:12.5px 0;">
			<a class="t-menu__link-item" href="#rec94249176">
				<div class="t607__tooltip t-descr t-descr_xs" style="background:#383838; line-height:2.5; border-radius:3px; color:#ffffff;font-size:14px;font-weight:600; right:40px;">Гифка недели</div>
				<div class="t607__dot" style=" border: 2px solid #dbdbdb; width:20px; height:20px; box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.1); "></div>
			</a>
		</li>
				<li class="t607__list_item" style="padding:12.5px 0;">
			<a class="t-menu__link-item" href="#rec34760814">
				<div class="t607__tooltip t-descr t-descr_xs" style="background:#383838; line-height:2.5; border-radius:3px; color:#ffffff;font-size:14px;font-weight:600; right:40px;">Медиатека</div>
				<div class="t607__dot" style=" border: 2px solid #dbdbdb; width:20px; height:20px; box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.1); "></div>
			</a>
		</li>
			</ul>
	</div>

<style>
#rec34714156 .t607__tooltip::after{
border-left-color: #383838;
}
#rec34714156 .t-menu__link-item.t-active .t607__dot{
background: #ce4d46 !important;opacity: 1 !important;
  
}
</style>

<script type="text/javascript">

    $(document).ready(function() {
      setTimeout(function() { t607_init('34714156'); }, 100);
    });

</script>

</div>


<div id="rec34758860" class="r t-rec t-rec_pb_0" style="padding-bottom:0px; " data-animationappear="off" data-record-type="217"   >
<div class="t190" style="position:fixed; z-index:100000; bottom:5px; left:30px; min-height:30px;">
  <a href="javascript:t190_scrollToTop();">
      
      
      
  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
       width="50px" height="50px" viewBox="0 0 48 48" enable-background="new 0 0 48 48" xml:space="preserve">
  <path style="fill:#ce4d46;" d="M43.006,47.529H4.964c-2.635,0-4.791-2.156-4.791-4.791V4.697c0-2.635,2.156-4.791,4.791-4.791h38.042
      c2.635,0,4.791,2.156,4.791,4.791v38.042C47.797,45.373,45.641,47.529,43.006,47.529z M25.503,16.881l6.994,7.049
      c0.583,0.588,1.532,0.592,2.121,0.008c0.588-0.583,0.592-1.533,0.008-2.122l-9.562-9.637c-0.281-0.283-0.664-0.443-1.063-0.443
      c0,0,0,0-0.001,0c-0.399,0-0.782,0.159-1.063,0.442l-9.591,9.637c-0.584,0.587-0.583,1.537,0.005,2.121
      c0.292,0.292,0.675,0.437,1.058,0.437c0.385,0,0.77-0.147,1.063-0.442L22.5,16.87v19.163c0,0.828,0.671,1.5,1.5,1.5
      s1.5-0.672,1.5-1.5L25.503,16.881z"/>
  </svg>
      
  </a>
</div>

<script type="text/javascript">

  $(document).ready(function(){
    $('.t190').css("display","none");
    $(window).bind('scroll', t_throttle(function(){
      if ($(window).scrollTop() > $(window).height()) {
         if($('.t190').css('display')=="none"){$('.t190').css("display","block");}
       }else{
         if($('.t190').css('display')=="block"){$('.t190').css("display","none");}           
       }
    }, 200));  
  });  

</script>

</div>

<!--footer-->
<div id="t-footer" class="t-records" data-hook="blocks-collection-content-node" data-tilda-project-id="443196" data-tilda-page-id="1979141" data-tilda-page-alias="footer" data-tilda-formskey="837688c9ca48e8cc3a88abe8957de2ba"   >

<div id="rec39046303" class="r t-rec t-rec_pt_45 t-rec_pb_45" style="padding-top:45px;padding-bottom:45px; " data-animationappear="off" data-record-type="420"   >
<!-- T420 -->



<div class="t420">
  <div class="t-container t-align_left">
    <div class="t420__col t-col t-col_3">
      <img src="https://static.tildacdn.com/tild3132-6566-4437-b735-353734346438/_.svg" class="t420__logo" imgfield="img"  alt="Разделы">      <div class="t-sociallinks">
  <div class="t-sociallinks__wrapper">
      <div class="t-sociallinks__item">
      <a href="#" target="_blank">
                  <svg class="t-sociallinks__svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px" height="25px" viewBox="0 0 48 48" enable-background="new 0 0 48 48" xml:space="preserve"><desc>Facebook</desc><path style="fill:#525252;" d="M47.761,24c0,13.121-10.638,23.76-23.758,23.76C10.877,47.76,0.239,37.121,0.239,24c0-13.124,10.638-23.76,23.764-23.76C37.123,0.24,47.761,10.876,47.761,24 M20.033,38.85H26.2V24.01h4.163l0.539-5.242H26.2v-3.083c0-1.156,0.769-1.427,1.308-1.427h3.318V9.168L26.258,9.15c-5.072,0-6.225,3.796-6.225,6.224v3.394H17.1v5.242h2.933V38.85z"/></svg>
              </a>
    </div>
          <div class="t-sociallinks__item">
      <a href="#" target="_blank">
                  <svg class="t-sociallinks__svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px" height="25px" viewBox="0 0 48 48" enable-background="new 0 0 48 48" xml:space="preserve"><desc>VK</desc><path style="fill:#525252;" d="M47.761,24c0,13.121-10.639,23.76-23.76,23.76C10.878,47.76,0.239,37.121,0.239,24c0-13.123,10.639-23.76,23.762-23.76C37.122,0.24,47.761,10.877,47.761,24 M35.259,28.999c-2.621-2.433-2.271-2.041,0.89-6.25c1.923-2.562,2.696-4.126,2.45-4.796c-0.227-0.639-1.64-0.469-1.64-0.469l-4.71,0.029c0,0-0.351-0.048-0.609,0.106c-0.249,0.151-0.414,0.505-0.414,0.505s-0.742,1.982-1.734,3.669c-2.094,3.559-2.935,3.747-3.277,3.524c-0.796-0.516-0.597-2.068-0.597-3.171c0-3.449,0.522-4.887-1.02-5.259c-0.511-0.124-0.887-0.205-2.195-0.219c-1.678-0.016-3.101,0.007-3.904,0.398c-0.536,0.263-0.949,0.847-0.697,0.88c0.31,0.041,1.016,0.192,1.388,0.699c0.484,0.656,0.464,2.131,0.464,2.131s0.282,4.056-0.646,4.561c-0.632,0.347-1.503-0.36-3.37-3.588c-0.958-1.652-1.68-3.481-1.68-3.481s-0.14-0.344-0.392-0.527c-0.299-0.222-0.722-0.298-0.722-0.298l-4.469,0.018c0,0-0.674-0.003-0.919,0.289c-0.219,0.259-0.018,0.752-0.018,0.752s3.499,8.104,7.573,12.23c3.638,3.784,7.764,3.36,7.764,3.36h1.867c0,0,0.566,0.113,0.854-0.189c0.265-0.288,0.256-0.646,0.256-0.646s-0.034-2.512,1.129-2.883c1.15-0.36,2.624,2.429,4.188,3.497c1.182,0.812,2.079,0.633,2.079,0.633l4.181-0.056c0,0,2.186-0.136,1.149-1.858C38.281,32.451,37.763,31.321,35.259,28.999"/></svg>
              </a>
    </div>
        <div class="t-sociallinks__item">
      <a href="#" target="_blank">
                  <svg class="t-sociallinks__svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="25px" height="25px" viewBox="0 0 30 30" xml:space="preserve"><desc>Odnoklassniki</desc><path style="fill:#525252;" d="M15.0010773,29.9969026 C6.71476662,29.9969026 -0.000942760943,23.2811932 -0.000942760943,14.9979126 C-0.000942760943,6.71261195 6.71476662,-0.00107736853 15.0010773,-0.00107736853 C23.2823378,-0.00107736853 29.9990572,6.71261195 29.9990572,14.9979126 C29.9990572,23.2811932 23.2833479,29.9969026 15.0010773,29.9969026 Z M14.9990572,9.16346042 C15.9845639,9.16346042 16.7857961,9.96559598 16.7857961,10.9501993 C16.7857961,11.9366092 15.9845639,12.7378415 14.9990572,12.7378415 C14.0126473,12.7378415 13.2114151,11.9366092 13.2114151,10.9501993 C13.2114151,9.96378937 14.0126473,9.16346042 14.9990572,9.16346042 L14.9990572,9.16346042 L14.9990572,9.16346042 Z M19.3159557,10.9501993 C19.3159557,8.56908521 17.3810746,6.63149426 14.9990572,6.63149426 C12.6170398,6.63149426 10.6803522,8.56908521 10.6803522,10.9501993 C10.6803522,13.3313134 12.6170398,15.2670977 14.9990572,15.2670977 C17.3810746,15.2670977 19.3159557,13.3313134 19.3159557,10.9501993 Z M16.7451474,18.7899904 C17.6249672,18.5876499 18.472268,18.242587 19.2527243,17.752092 C19.8443896,17.3790267 20.0232441,16.5976672 19.6510821,16.0069052 C19.2771135,15.4152399 18.4985739,15.2363853 17.9058953,15.6085473 C16.1354159,16.7214201 13.8590853,16.7214201 12.0904126,15.6085473 C11.4987473,15.2372886 10.7164844,15.4152399 10.3461291,16.0069052 C9.97306376,16.5967639 10.1519183,17.3790267 10.741777,17.752092 C11.5222332,18.2416837 12.3704374,18.5903598 13.2493539,18.7899904 L10.8348175,21.2036235 C10.3416125,21.6977317 10.3416125,22.5007706 10.8348175,22.9930722 C11.0805167,23.2387714 11.4048035,23.3634276 11.7290902,23.3634276 C12.053377,23.3634276 12.3776638,23.2387714 12.6242663,22.9930722 L14.995444,20.6209912 L17.3684284,22.9939756 C17.8625366,23.4871805 18.6637689,23.4871805 19.1560705,22.9939756 C19.6519854,22.4998673 19.6519854,21.6968284 19.1560705,21.2054301 L16.7451474,18.7899904 L16.7451474,18.7899904 L16.7451474,18.7899904 Z"></path></svg>
              </a>
    </div>
            <div class="t-sociallinks__item">
      <a href="#" target="_blank">
                  <svg class="t-sociallinks__svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px" height="25px" viewBox="0 0 48 48" enable-background="new 0 0 48 48" xml:space="preserve"><desc>Youtube</desc><path style="fill:#525252;" d="M24 0.0130005C37.248 0.0130005 47.987 10.753 47.987 24C47.987 37.247 37.247 47.987 24 47.987C10.753 47.987 0.0130005 37.247 0.0130005 24C0.0130005 10.753 10.752 0.0130005 24 0.0130005ZM35.815 18.093C35.565 16.756 34.452 15.758 33.173 15.635C30.119 15.439 27.054 15.28 23.995 15.278C20.936 15.276 17.882 15.432 14.828 15.625C13.544 15.749 12.431 16.742 12.182 18.084C11.898 20.017 11.756 21.969 11.756 23.92C11.756 25.871 11.898 27.823 12.182 29.756C12.431 31.098 13.544 32.21 14.828 32.333C17.883 32.526 20.935 32.723 23.995 32.723C27.053 32.723 30.121 32.551 33.173 32.353C34.452 32.229 35.565 31.084 35.815 29.747C36.101 27.817 36.244 25.868 36.244 23.919C36.244 21.971 36.101 20.023 35.815 18.093ZM21.224 27.435V20.32L27.851 23.878L21.224 27.435Z"/></svg>
              </a>
    </div>
        <div class="t-sociallinks__item">
      <a href="#" target="_blank">
                  <svg class="t-sociallinks__svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="25px" height="25px" viewBox="0 0 30 30" xml:space="preserve"><desc>Instagram</desc><path style="fill:#525252;" d="M15,11.014 C12.801,11.014 11.015,12.797 11.015,15 C11.015,17.202 12.802,18.987 15,18.987 C17.199,18.987 18.987,17.202 18.987,15 C18.987,12.797 17.199,11.014 15,11.014 L15,11.014 Z M15,17.606 C13.556,17.606 12.393,16.439 12.393,15 C12.393,13.561 13.556,12.394 15,12.394 C16.429,12.394 17.607,13.561 17.607,15 C17.607,16.439 16.444,17.606 15,17.606 L15,17.606 Z"></path><path style="fill:#525252;" d="M19.385,9.556 C18.872,9.556 18.465,9.964 18.465,10.477 C18.465,10.989 18.872,11.396 19.385,11.396 C19.898,11.396 20.306,10.989 20.306,10.477 C20.306,9.964 19.897,9.556 19.385,9.556 L19.385,9.556 Z"></path><path style="fill:#525252;" d="M15.002,0.15 C6.798,0.15 0.149,6.797 0.149,15 C0.149,23.201 6.798,29.85 15.002,29.85 C23.201,29.85 29.852,23.202 29.852,15 C29.852,6.797 23.201,0.15 15.002,0.15 L15.002,0.15 Z M22.666,18.265 C22.666,20.688 20.687,22.666 18.25,22.666 L11.75,22.666 C9.312,22.666 7.333,20.687 7.333,18.28 L7.333,11.734 C7.333,9.312 9.311,7.334 11.75,7.334 L18.25,7.334 C20.688,7.334 22.666,9.312 22.666,11.734 L22.666,18.265 L22.666,18.265 Z"></path></svg>
              </a>
    </div>
              <div class="t-sociallinks__item">
      <a href="#" target="_blank">
                  <svg class="t-sociallinks__svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="25px" height="25px" viewBox="0 0 60 60" xml:space="preserve"><desc>Telegram</desc><path style="fill:#525252;" d="M30 0C13.4 0 0 13.4 0 30s13.4 30 30 30 30-13.4 30-30S46.6 0 30 0zm16.9 13.9l-6.7 31.5c-.1.6-.8.9-1.4.6l-10.3-6.9-5.5 5.2c-.5.4-1.2.2-1.4-.4L18 32.7l-9.5-3.9c-.7-.3-.7-1.5 0-1.8l37.1-14.1c.7-.2 1.4.3 1.3 1z"/><path style="fill:#525252;" d="M22.7 40.6l.6-5.8 16.8-16.3-20.2 13.3"/></svg>
              </a>
    </div>
    
  </div>
</div>
      <div class="t420__text t-descr t-descr_xxs" style="" field="text"><div style="font-family:'Open Sans';" data-customstyle="yes">© 2009-2019 Лекториум</div></div>    </div>
    <div class="t420__col t-col t-col_3">
      <div class="t420__title t-name t-name_xs t420__title_uppercase" field="title" style="color: #525252;">Разделы</div>      <div class="t420__descr t-descr t-descr_xxs" field="descr" style="color: #000000;"><div style="font-family:'Open Sans';color:#000000;" data-customstyle="yes"><ul> <li></li><li><a href="#" style="color:#000000 !important;"><span style="font-weight: 500;">Онлайн-курсы</span></a></li> <li><a href="#" style="color: rgb(0, 0, 0);"><span style="font-weight: 500;">Медиатека</span></a></li> <li><span style="font-weight: 500;"><a href="#" style="color:#000000 !important;">Спецпроекты</a></span></li><li><a href="#" style="color: rgb(0, 0, 0);"><span style="font-weight: 500;">Форум</span></a></li><li><span style="color: rgb(0, 0, 0); font-weight: 500;"><a href="#" style="">Конференция Парсек</a></span></li><li></li></ul></div></div>    </div>
    <div class="t420__floatbeaker_lr3"></div>
    <div>
    <div class="t420__col t-col t-col_3">
      <div class="t420__title t-name t-name_xs t420__title_uppercase" field="title2" style="color: #525252;">Партнерам</div>      <div class="t420__descr t-descr t-descr_xxs" field="descr2" style="color: #000000;"><div style="font-family:'Open Sans';color:#000000;" data-customstyle="yes"><ul><li></li><li><a href="#" style="color:#000000 !important;"><span style="font-weight: 500;">Издательство</span></a></li><li><span style="font-weight: 500;"><a href="#" style="color: rgb(0, 0, 0);">Видеосъемка</a></span></li><li><span style="font-weight: 400;"><a href="#" style="">Обучение сотрудников</a></span></li><li>
		<span style="font-weight: 500;"><a href="#">Платформа Эдуардо</a></span>
	</li><li><a href="#" style="color: rgb(0, 0, 0);"><span style="font-weight: 500;">Медиагранты</span></a></li><li><a href="#" style="color:#000000 !important;"><span style="font-weight: 500;">Публикация</span></a></li><li><a href="#popup:pr" style="color:#000000 !important;"><span style="font-weight: 500;">Реклама</span></a></li></ul></div></div>    </div>
    <div class="t420__col t-col t-col_3">
      <div class="t420__title t-name t-name_xs t420__title_uppercase" field="title3" style="color: #525252;">Инфо</div>      <div class="t420__descr t-descr t-descr_xxs" field="descr3" style="color: #000000;"><div style="font-family:'Open Sans';color:#000000;" data-customstyle="yes"><ul><li></li><li><a href="#" style="color:#000000 !important;"><span style="font-weight: 400;">О Лекториуме</span></a></li><li><a href="#" style="color:#000000 !important;"><span style="font-weight: 500;">Вакансии</span></a></li><li><a href="#popup:aid" style="color:#000000 !important;"><span style="font-weight: 400;">Поддержать проект</span></a></li><li><span style="font-weight: 500;"><a href="#" style="color: rgb(0, 0, 0);">Правовая информация</a></span></li><li><span style="font-weight: 500;"><a href="#" style="color: rgb(0, 0, 0);"><u></u></a></span><a href="#popup:contacts" style=""><span style="font-weight: 400;">Контакты</span></a></li><li><span style="font-weight: 500;"><a href="#" style="color:#000000 !important;">Оферта</a></span></li><li><a href="#" target="_blank" style=""><span style="font-weight: 400;">Согласие на обработку ПД</span></a></li><li><a href="#" style=""><span style="font-weight: 400;">Логотипы</span></a></li></ul></div></div>    </div>
      </div>
  </div>
</div>
</div>


<div id="rec41418919" class="r t-rec" style="background-color:#ffffff; "  data-record-type="390"   data-bg-color="#ffffff">


<div class="t390">
  <div class="t-popup" data-tooltip-hook="#popup:aid"  >
    <div class="t-popup__close">
      <div class="t-popup__close-wrapper">
      <svg class="t-popup__close-icon" width="23px" height="23px" viewBox="0 0 23 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <g stroke="none" stroke-width="1" fill="#fff" fill-rule="evenodd">
          <rect transform="translate(11.313708, 11.313708) rotate(-45.000000) translate(-11.313708, -11.313708) " x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
          <rect transform="translate(11.313708, 11.313708) rotate(-315.000000) translate(-11.313708, -11.313708) " x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
        </g>
      </svg>
      </div>
    </div>
    <div class="t-popup__container t-width t-width_6" style="background-color: #fbfbf9;">
            <div class="t390__wrapper t-align_center">
        <div class="t390__uptitle t-uptitle t-uptitle_xs" style=""><div style="font-family:'Intro';" data-customstyle="yes">Есть три способа</div></div>        <div class="t390__title t-heading t-heading_lg" style=""><div style="font-family:'Intro';" data-customstyle="yes">Поддержать Лекториум</div></div>        <div class="t390__descr t-descr t-descr_xs" style=""><div style="font-family:'Open Sans';" data-customstyle="yes">У нас нет рекламного бюджета, все деньги мы тратим на создание качественного контента, чтобы сделать знание доступным. Расскажите о нас своим друзьям и знакомым.<br /><br />Мы бесплатно записываем академические курсы, это происходит на деньги меценатов. Если вы хотите поддержать нас, реквизиты есть <a href="#" style=""><span style="color: rgb(206, 77, 70);"><u>тут</u></span></a>.<br /><br />Если вы готовы профинансировать создание онлайн-курса или целого направления, пишите на <span style="color: rgb(206, 77, 70);"><u>somov@lektorium.tv</u></span>.<br /></div></div>                      </div>
    </div>
  </div>
</div>



<script type="text/javascript">

$(document).ready(function(){
  $("#rec41418919").attr('data-animationappear','off');
  $("#rec41418919").css('opacity','1');
  setTimeout(function(){
    t390_initPopup('41418919');
  }, 500);
});
</script>



<style>

#rec41418919 .t-btn[data-btneffects-first],
#rec41418919 .t-btn[data-btneffects-second],
#rec41418919 .t-submit[data-btneffects-first],
#rec41418919 .t-submit[data-btneffects-second] {
	position: relative;
  overflow: hidden;
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
}





</style>



<script type="text/javascript">

$(document).ready(function() {
	

	

});

</script>
</div>


<div id="rec41933400" class="r t-rec" style="background-color:#ffffff; "  data-record-type="390"   data-bg-color="#ffffff">


<div class="t390">
  <div class="t-popup" data-tooltip-hook="#popup:pr"  >
    <div class="t-popup__close">
      <div class="t-popup__close-wrapper">
      <svg class="t-popup__close-icon" width="23px" height="23px" viewBox="0 0 23 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <g stroke="none" stroke-width="1" fill="#fff" fill-rule="evenodd">
          <rect transform="translate(11.313708, 11.313708) rotate(-45.000000) translate(-11.313708, -11.313708) " x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
          <rect transform="translate(11.313708, 11.313708) rotate(-315.000000) translate(-11.313708, -11.313708) " x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
        </g>
      </svg>
      </div>
    </div>
    <div class="t-popup__container t-width t-width_6" style="background-color: #fbfbf9;">
            <div class="t390__wrapper t-align_center">
        <div class="t390__uptitle t-uptitle t-uptitle_xs" style=""><div style="font-family:'Intro';" data-customstyle="yes">О рекламе</div></div>        <div class="t390__title t-heading t-heading_lg" style=""><div style="font-family:'Intro';" data-customstyle="yes"><div style="font-family:'Intro';" data-customstyle="yes"></div></div></div>        <div class="t390__descr t-descr t-descr_xs" style=""><div style="font-family:'Open Sans';" data-customstyle="yes">Как вы видите, на Лекториуме практически нет рекламы. <br />Мы считаем, что создание контента — лучший способ продвижения. Лучше записать отличную конференцию в рамках <a href="#"><span style="color: rgb(206, 77, 70);"><u>программы Медиагранты</u></span></a> и повесить на нее титр спонсора, чем вешать растяжки и плакаты. <br />Поэтому мы не любим баннеры и это будет отражено в прайсе на размещение. Однако, если вас это не останавливает — напишите нам  письмо на office@lektorium.tv.<br /></div></div>                      </div>
    </div>
  </div>
</div>



<script type="text/javascript">

$(document).ready(function(){
  $("#rec41933400").attr('data-animationappear','off');
  $("#rec41933400").css('opacity','1');
  setTimeout(function(){
    t390_initPopup('41933400');
  }, 500);
});
</script>



<style>

#rec41933400 .t-btn[data-btneffects-first],
#rec41933400 .t-btn[data-btneffects-second],
#rec41933400 .t-submit[data-btneffects-first],
#rec41933400 .t-submit[data-btneffects-second] {
	position: relative;
  overflow: hidden;
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
}





</style>



<script type="text/javascript">

$(document).ready(function() {
	

	

});

</script>
</div>


<div id="rec41418977" class="r t-rec" style="background-color:#ffffff; "  data-record-type="390"   data-bg-color="#ffffff">


<div class="t390">
  <div class="t-popup" data-tooltip-hook="#popup:contacts"  >
    <div class="t-popup__close">
      <div class="t-popup__close-wrapper">
      <svg class="t-popup__close-icon" width="23px" height="23px" viewBox="0 0 23 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <g stroke="none" stroke-width="1" fill="#fff" fill-rule="evenodd">
          <rect transform="translate(11.313708, 11.313708) rotate(-45.000000) translate(-11.313708, -11.313708) " x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
          <rect transform="translate(11.313708, 11.313708) rotate(-315.000000) translate(-11.313708, -11.313708) " x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
        </g>
      </svg>
      </div>
    </div>
    <div class="t-popup__container t-width t-width_6" style="background-color: #fbfbf9;">
            <div class="t390__wrapper t-align_center">
                        <div class="t390__descr t-descr t-descr_xs" style="">По всем вопросам пишите на <span style="color: rgb(206, 77, 70);"><u>support@lektorium.tv</u></span><br /><span style="color: rgb(0, 0, 0);">По вопросам сотрудничества обращайтесь на </span> <a href="mailto:office@lektorium.tv"><span style="color: rgb(206, 77, 70);"><u>office@lektorium.tv</u></span></a><u><span style="color: rgb(206, 77, 70);" data-redactor-tag="span"><u></u></span></u></div>                      </div>
    </div>
  </div>
</div>



<script type="text/javascript">

$(document).ready(function(){
  $("#rec41418977").attr('data-animationappear','off');
  $("#rec41418977").css('opacity','1');
  setTimeout(function(){
    t390_initPopup('41418977');
  }, 500);
});
</script>



<style>

#rec41418977 .t-btn[data-btneffects-first],
#rec41418977 .t-btn[data-btneffects-second],
#rec41418977 .t-submit[data-btneffects-first],
#rec41418977 .t-submit[data-btneffects-second] {
	position: relative;
  overflow: hidden;
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
}





</style>



<script type="text/javascript">

$(document).ready(function() {
	

	

});

</script>
</div>


<div id="rec41418981" class="r t-rec" style="background-color:#ffffff; " data-animationappear="off" data-record-type="702"   data-bg-color="#ffffff">
<!-- T702 -->
<div class="t702">
  <div class="t-popup" data-tooltip-hook="#popup:support"  >
    <div class="t-popup__close">
      <div class="t-popup__close-wrapper">
        <svg class="t-popup__close-icon" width="23px" height="23px" viewBox="0 0 23 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
          <g stroke="none" stroke-width="1" fill="#fff" fill-rule="evenodd">
            <rect transform="translate(11.313708, 11.313708) rotate(-45.000000) translate(-11.313708, -11.313708) " x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
            <rect transform="translate(11.313708, 11.313708) rotate(-315.000000) translate(-11.313708, -11.313708) " x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
          </g>
        </svg>
      </div>  
    </div>
    <div class="t-popup__container t-width t-width_6" >
                <div class="t702__wrapper">
          <div class="t702__text-wrapper t-align_center">
            <div class="t702__title t-title t-title_xxs" style="">Напишите нам</div>            <div class="t702__descr t-descr t-descr_xs" style=""><div style="font-family:'Open Sans';" data-customstyle="yes">Если есть вопрос, нашли опечатку или что-то не работает<br /></div></div>          </div>  
                                    	  		                  	  		                  	  		
    <form id="form41418981" name='form41418981' role="form" action='' method='POST' data-formactiontype="2" data-inputbox=".t-input-group"   class="t-form js-form-proccess t-form_inputs-total_3 t-form_bbonly "  data-success-callback="t702_onSuccess"  >
	
	  			        	            	                <input type="hidden" name="formservices[]" value="7d89bc9c04e022ffa05d81dab21170ed" class="js-formaction-services">
	            	        
            
            <div class="js-successbox t-form__successbox t-text t-text_md" style="display:none;">Спасибо! Как правило, мы отвечаем в течение одного рабочего дня.</div>

            <div class="t-form__inputsbox">

				
            <div class="t-input-group t-input-group_em" data-input-lid="1495810354468">
                              <div class="t-input-block">
                          <input type="text" name="Email" class="t-input js-tilda-rule t-input_bbonly" value="" placeholder="Электропочта" data-tilda-req="1" data-tilda-rule="email" style="color:#000000; border:1px solid #c9c9c9;  border-radius: 0px; -moz-border-radius: 0px; -webkit-border-radius: 0px;">
                    <div class="t-input-error"></div>
          </div>
      </div>


            <div class="t-input-group t-input-group_nm" data-input-lid="1495810359387">
                              <div class="t-input-block">
                          <input type="text" name="Name" class="t-input js-tilda-rule t-input_bbonly" value="" placeholder="Имя" data-tilda-req="1" data-tilda-rule="name" style="color:#000000; border:1px solid #c9c9c9;  border-radius: 0px; -moz-border-radius: 0px; -webkit-border-radius: 0px;">
                    <div class="t-input-error"></div>
          </div>
      </div>


            <div class="t-input-group t-input-group_in" data-input-lid="1508338764854">
                              <div class="t-input-block">
                          <input type="text" name="Input" class="t-input js-tilda-rule  t-input_bbonly" value=""     style="color:#000000; border:1px solid #c9c9c9;  border-radius: 0px; -moz-border-radius: 0px; -webkit-border-radius: 0px;">
                    <div class="t-input-error"></div>
          </div>
      </div>





				<div class="t-form__errorbox-middle">
					                <div class="js-errorbox-all t-form__errorbox-wrapper" style="display:none;">
                    <div class="t-form__errorbox-text t-text t-text_md">
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-all"></p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-req"></p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-email"></p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-name"></p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-phone"></p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-minlength"></p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-string"></p>
                    </div>
                </div>				</div>

                <div class="t-form__submit">
                    <button type="submit" class="t-submit" style="color:#ffffff;background-color:#000000;border-radius:0px; -moz-border-radius:0px; -webkit-border-radius:0px;" >Отправить</button>
                </div>

            </div>

            <div class="t-form__errorbox-bottom">
            	                <div class="js-errorbox-all t-form__errorbox-wrapper" style="display:none;">
                    <div class="t-form__errorbox-text t-text t-text_md">
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-all"></p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-req"></p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-email"></p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-name"></p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-phone"></p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-minlength"></p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-string"></p>
                    </div>
                </div>            </div>


    </form>


		
	<style>
	#rec41418981 input::-webkit-input-placeholder {color:#000000; opacity: 0.5;}
	#rec41418981 input::-moz-placeholder          {color:#000000; opacity: 0.5;}
	#rec41418981 input:-moz-placeholder           {color:#000000; opacity: 0.5;}
	#rec41418981 input:-ms-input-placeholder      {color:#000000; opacity: 0.5;}
	#rec41418981 textarea::-webkit-input-placeholder {color:#000000; opacity: 0.5;}
	#rec41418981 textarea::-moz-placeholder          {color:#000000; opacity: 0.5;}
	#rec41418981 textarea:-moz-placeholder           {color:#000000; opacity: 0.5;}
	#rec41418981 textarea:-ms-input-placeholder      {color:#000000; opacity: 0.5;}
	</style>
	
	                  </div>
      </div>
    </div>
</div>



<script type="text/javascript">

$(document).ready(function(){
  setTimeout(function(){
    t702_initPopup('41418981');
  }, 500);
  
    /* hack for Android */
    var ua = navigator.userAgent.toLowerCase();
    var isAndroid = ua.indexOf("android") > -1;
    if(isAndroid) {
        $('.t-body').addClass('t-body_scrollable-hack-for-android');
        $('head').append("<style>@media screen and (max-width: 560px) {\n.t-body_scrollable-hack-for-android {\noverflow: visible !important;\n}\n}\n</style>");
        console.log('Android css hack was inited');
    }
});
</script>




<style>

#rec41418981 .t-btn[data-btneffects-first],
#rec41418981 .t-btn[data-btneffects-second],
#rec41418981 .t-submit[data-btneffects-first],
#rec41418981 .t-submit[data-btneffects-second] {
	position: relative;
  overflow: hidden;
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
}





</style>



<script type="text/javascript">

$(document).ready(function() {
	

	

});

</script>
</div>

</div>
<!--/footer-->

</div>
<!--/allrecords-->


	
	

	<!-- Yandex.Metrika counter 24232456 --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter24232456 = new Ya.Metrika({id:24232456, webvisor:true, clickmap:true, trackLinks:true,accurateTrackBounce:true,ecommerce:true}); w.mainMetrika = 'yaCounter24232456'; } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/24232456" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
	
	<script type="text/javascript">
	
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-48480152-1', 'auto');
	
	
	
	ga('send', 'pageview');
	window.mainTracker = 'user';
	
	if (! window.mainTracker) { window.mainTracker = 'tilda'; }

    
	(function (d, w, k, o, g) { var n=d.getElementsByTagName(o)[0],s=d.createElement(o),f=function(){n.parentNode.insertBefore(s,n);}; s.type = "text/javascript"; s.async = true; s.key = k; s.id = "tildastatscript"; s.src=g; if (w.opera=="[object Opera]") {d.addEventListener("DOMContentLoaded", f, false);} else { f(); } })(document, window, '84d95675d1a902a52c0a9d3e52fe7647','script','https://stat.tildacdn.com/js/tildastat-0.2.min.js');


	
	</script>
	
</body>
</html>