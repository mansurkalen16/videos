@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Добро пожаловать</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        Вы вошли!
                        @if(!auth()->user()->paid && auth()->user()->role_id == 3)
                            Вы не оплатили!
                            <a href="{{ action('UserController@pay')}}" class="btn btn-success">Оплатить</a>
                        @elseif(auth()->user()->paid == 1 && auth()->user()->role_id == 3)
                            Ваш запрос на оплату в разработке!
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
