<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['auth', 'paid']], function() {
    Route::get('/videos', 'VideoController@index');
    Route::get('/videos/create', 'VideoController@create');
    Route::get('/videos/{id}', 'VideoController@show');
    Route::post('/videos', 'VideoController@store');
    Route::delete('/videos/{id}', 'VideoController@destroy');

    Route::get('/courses/create', 'CourseController@create');
    Route::get('/courses', 'CourseController@index');
    Route::get('/courses/{id}', 'CourseController@show');
    Route::post('/courses', 'CourseController@store');
    Route::delete('/courses/{id}', 'CourseController@destroy');

    Route::get('/users', 'UserController@index');
    Route::get('/users/paid/{id}', 'UserController@paid');
    Route::get('/users/pay', 'UserController@pay');
});